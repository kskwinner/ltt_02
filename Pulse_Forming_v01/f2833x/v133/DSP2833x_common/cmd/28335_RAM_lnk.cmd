/*
// TI File $Revision: /main/11 $
// Checkin $Date: April 15, 2009   09:57:28 $
//###########################################################################
//
// FILE:    28335_RAM_lnk.cmd
//
// TITLE:   Linker Command File For 28335 examples that run out of RAM
//
//          This ONLY includes all SARAM blocks on the 28335 device.
//          This does not include flash or OTP.
//
//          Keep in mind that L0 and L1 are protected by the code
//          security module.
//
//          What this means is in most cases you will want to move to
//          another memory map file which has more memory defined.
//
//###########################################################################
// $TI Release:   $
// $Release Date:   $
//###########################################################################
*/

/* ======================================================
// For Code Composer Studio V2.2 and later
// ---------------------------------------
// In addition to this memory linker command file,
// add the header linker command file directly to the project.
// The header linker command file is required to link the
// peripheral structures to the proper locations within
// the memory map.
//
// The header linker files are found in <base>\DSP2833x_Headers\cmd
//
// For BIOS applications add:      DSP2833x_Headers_BIOS.cmd
// For nonBIOS applications add:   DSP2833x_Headers_nonBIOS.cmd
========================================================= */

/* ======================================================
// For Code Composer Studio prior to V2.2
// --------------------------------------
// 1) Use one of the following -l statements to include the
// header linker command file in the project. The header linker
// file is required to link the peripheral structures to the proper
// locations within the memory map                                    */

/* Uncomment this line to include file only for non-BIOS applications */
/* -l DSP2833x_Headers_nonBIOS.cmd */

/* Uncomment this line to include file only for BIOS applications */
/* -l DSP2833x_Headers_BIOS.cmd */

/* 2) In your project add the path to <base>\DSP2833x_headers\cmd to the
   library search path under project->build options, linker tab,
   library search path (-i).
/*========================================================= */

/* Define the memory block start/length for the F28335
   PAGE 0 will be used to organize program sections
   PAGE 1 will be used to organize data sections

   Notes:
         Memory blocks on F28335 are uniform (ie same
         physical memory) in both PAGE 0 and PAGE 1.
         That is the same memory region should not be
         defined for both PAGE 0 and PAGE 1.
         Doing so will result in corruption of program
         and/or data.

         L0/L1/L2 and L3 memory blocks are mirrored - that is
         they can be accessed in high memory or low memory.
         For simplicity only one instance is used in this
         linker file.

         Contiguous SARAM memory blocks can be combined
         if required to create a larger memory block.
*/

/*
Contiguous SARAM memory blocks can be combined
if required to create a larger memory block.
*/
/*________________________________________________________________*/
/*
F28335 Memory MAP
	Block
	Start		Data	Program
	Address		Space	Space
	0x00 0000	X		X		M0 Vector - RAM (32 x 32)(Enable if VMAP = 0)
	0x00 0040	O		O		M0 SARAM (1Kx16)
	0x00 0400	O		O		M1 SARAM (1Kx16)
	0x00 0800	O		X		Peripheral Frame 0
	0x00 0D00	X		X		PIE Vector - RAM (256x16) (Enabled if VMAP = 1, ENPIE =1)
	0x00 2000	X		X		Reserved
	0x00 4000	O		O		XINTF Zone 0 (4K x 16, XZCS0 ) (Protected) DMA Accessible
	0x00 5000	O		X		Peripheral Frame 3 (Protected) DMA Accessible
	0x00 6000	O		X		Peripheral Frame 1 (Protected)
	0x00 7000	O		X		Peripheral Frame 2 (Protected)
	0x00 8000	O		O		L0 SARAM (4Kx16, Secure Zone, Dual Mapped)
	0x00 9000	O		O		L1 SARAM (4Kx16, Secure Zone, Dual Mapped)
	0x00 A000	O		O		L2 SARAM (4Kx16, Secure Zone, Dual Mapped)
	0x00 B000	O		O		L3 SARAM (4Kx16, Secure Zone, Dual Mapped)
	0x00 C000	O		O		L4 SARAM (4Kx16, DMA Accessible)
	0x00 D000	O		O		L5 SARAM (4Kx16, DMA Accessible)
	0x00 E000	O		O		L6 SARAM (4Kx16, DMA Accessible)
	0x00 F000	O		O		L7 SARAM (4Kx16, DMA Accessible)
	0x01 0000	X		X		Reserved
	0x10 0000	O		O		XINTF Zone 6 (1 M x 16, XZCS6 ) (DMA Accessible)
	0x20 0000	O		O		XINTF Zone 7 (1 M x 16, XZCS7 ) (DMA Accessible)
	0x30 0000	O		O		FLASH (256 K x 16, Secure Zone)
	0x33 FFF8	O		O		128-bit Password
	0x34 0000	O		O		Reserved
	0x38 0080	O		O		ADC Calibration Data
	0x38 0090	O		O		Reserved
	0x38 0400	O		O		User OTP (1K x 16, Secure Zone)
	0x38 0800	X		X		Reserved
	0x3F 8000	O		O		L0 SARAM (4K x 16, Secure Zone Dual Mapped)
	0x3F 9000	O		O		L1 SARAM (4K x 16, Secure Zone Dual Mapped)
	0x3F A000	O		O		L2 SARAM (4K x 16, Secure Zone Dual Mapped)
	0x3F B000	O		O		L3 SARAM (4K x 16, Secure Zone Dual Mapped)
	0x3F C000	X		X		Reserved
	0x3F E000	O		O		Boot ROM (8K x 16)
	0x3F FFC0	O		O		BROM Vector - ROM (32 x 32) (Enable if VMAP = 1, ENPIE = 0)
*/
/*________________________________________________________________*/
/*________________________________________________________________*/


MEMORY
{
PAGE 0 :
   /* BEGIN is used for the "boot to SARAM" bootloader mode      */

   BEGIN      : origin = 0x000000, length = 0x000002     /* Boot to M0 will go here                      */
   RAMM0      : origin = 0x000050, length = 0x0003B0
   RAML0      : origin = 0x008000, length = 0x001000
   RAML1      : origin = 0x009000, length = 0x002000

   //RAML1      : origin = 0x009000, length = 0x001000
   //RAML2      : origin = 0x00A000, length = 0x001000
   RAML3      : origin = 0x00B000, length = 0x001000
   ZONE7A     : origin = 0x200000, length = 0x00FC00    /* XINTF zone 7 - program space */
   CSM_RSVD   : origin = 0x33FF80, length = 0x000076     /* Part of FLASHA.  Program with all 0x0000 when CSM is in use. */
   CSM_PWL    : origin = 0x33FFF8, length = 0x000008     /* Part of FLASHA.  CSM password locations in FLASHA            */
   ADC_CAL    : origin = 0x380080, length = 0x000009
   RESET      : origin = 0x3FFFC0, length = 0x000002
   IQTABLES   : origin = 0x3FE000, length = 0x000b50
   IQTABLES2  : origin = 0x3FEB50, length = 0x00008c
   FPUTABLES  : origin = 0x3FEBDC, length = 0x0006A0
   BOOTROM    : origin = 0x3FF27C, length = 0x000D44


PAGE 1 :
   /* BOOT_RSVD is used by the boot ROM for stack.               */
   /* This section is only reserved to keep the BOOT ROM from    */
   /* corrupting this area during the debug process              */

   BOOT_RSVD  : origin = 0x000002, length = 0x00004E     /* Part of M0, BOOT rom will use this for stack */
   RAMM1      : origin = 0x000400, length = 0x000400     /* on-chip RAM block M1 */
   RAML4      : origin = 0x00C000, length = 0x001000
   RAML5      : origin = 0x00D000, length = 0x001000
   RAML6      : origin = 0x00E000, length = 0x001000
   RAML7      : origin = 0x00F000, length = 0x001000
   ZONE7B     : origin = 0x20FC00, length = 0x000400     /* XINTF zone 7 - data space */
}


SECTIONS
{
   /* Setup for "boot to SARAM" mode:
      The codestart section (found in DSP28_CodeStartBranch.asm)
      re-directs execution to the start of user code.  */
   codestart        : > BEGIN,     PAGE = 0
   ramfuncs         : > RAML0,     PAGE = 0
   .text            : > RAML1,     PAGE = 0
   .cinit           : > RAML0,     PAGE = 0
   .pinit           : > RAML0,     PAGE = 0
   .switch          : > RAML0,     PAGE = 0

   .stack           : > RAMM1,     PAGE = 1
   .ebss            : > RAML4,     PAGE = 1
   .econst          : > RAML5,     PAGE = 1
   .esysmem         : > RAMM1,     PAGE = 1

   IQmath           : > RAML1,     PAGE = 0
   IQmathTables     : > IQTABLES,  PAGE = 0, TYPE = NOLOAD

   /* Uncomment the section below if calling the IQNexp() or IQexp()
      functions from the IQMath.lib library in order to utilize the
      relevant IQ Math table in Boot ROM (This saves space and Boot ROM
      is 1 wait-state). If this section is not uncommented, IQmathTables2
      will be loaded into other memory (SARAM, Flash, etc.) and will take
      up space, but 0 wait-state is possible.
   */
   /*
   IQmathTables2    : > IQTABLES2, PAGE = 0, TYPE = NOLOAD
   {

              IQmath.lib<IQNexpTable.obj> (IQmathTablesRam)

   }
   */

   FPUmathTables    : > FPUTABLES, PAGE = 0, TYPE = NOLOAD

   DMARAML4         : > RAML4,     PAGE = 1
   DMARAML5         : > RAML5,     PAGE = 1
   DMARAML6         : > RAML6,     PAGE = 1
   DMARAML7         : > RAML7,     PAGE = 1

   ZONE7DATA        : > ZONE7B,    PAGE = 1

   .reset           : > RESET,     PAGE = 0, TYPE = DSECT /* not used                    */
   csm_rsvd         : > CSM_RSVD   PAGE = 0, TYPE = DSECT /* not used for SARAM examples */
   csmpasswds       : > CSM_PWL    PAGE = 0, TYPE = DSECT /* not used for SARAM examples */

   /* Allocate ADC_cal function (pre-programmed by factory into TI reserved memory) */
   .adc_cal     : load = ADC_CAL,   PAGE = 0, TYPE = NOLOAD

}

/*
//===========================================================================
// End of file.
//===========================================================================
*/
