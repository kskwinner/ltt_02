//###########################################################################
//
// FILE:	DSP2833x_ECan.c
//
// TITLE:	DSP2833x Enhanced CAN Initialization & Support Functions.
//
//###########################################################################
// $TI Release: 2833x/2823x Header Files and Peripheral Examples V133 $
// $Release Date: June 8, 2012 $
//###########################################################################

#include "DSP2833x_Device.h"     // DSP2833x Headerfile Include File
#include "DSP2833x_Examples.h"   // DSP2833x Examples Include File


//---------------------------------------------------------------------------
// InitECan:
//---------------------------------------------------------------------------
// This function initializes the eCAN module to a known state.
//
void InitECan(void)
{
#if DSP28_ECANA
   InitECana();
#endif // if DSP28_ECANA

#if DSP28_ECANB
   InitECanb();
#endif // if DSP28_ECANB
}



void InitECana(void)		// Initialize eCAN-A module
{
/* Create a shadow register structure for the CAN control registers. This is
 needed, since only 32-bit access is allowed to these registers. 16-bit access
 to these registers could potentially corrupt the register contents or return
 false data. This is especially true while writing to/reading from a bit
 (or group of bits) among bits 16 - 31 */

struct ECAN_REGS ECanaShadow;

	EALLOW;		// EALLOW enables access to protected bits

/* Configure eCAN RX and TX pins for CAN operation using eCAN regs*/

    ECanaShadow.CANTIOC.all = ECanaRegs.CANTIOC.all;
    ECanaShadow.CANTIOC.bit.TXFUNC = 1;
    ECanaRegs.CANTIOC.all = ECanaShadow.CANTIOC.all;

    ECanaShadow.CANRIOC.all = ECanaRegs.CANRIOC.all;
    ECanaShadow.CANRIOC.bit.RXFUNC = 1;
    ECanaRegs.CANRIOC.all = ECanaShadow.CANRIOC.all;

/* Configure eCAN for HECC mode - (reqd to access mailboxes 16 thru 31) */
									// HECC mode also enables time-stamping feature

	ECanaShadow.CANMC.all = ECanaRegs.CANMC.all;
	ECanaShadow.CANMC.bit.SCB = 1;
	ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;

/* Initialize all bits of 'Master Control Field' to zero */
// Some bits of MSGCTRL register come up in an unknown state. For proper operation,
// all bits (including reserved bits) of MSGCTRL must be initialized to zero

    ECanaMboxes.MBOX0.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX1.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX2.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX3.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX4.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX5.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX6.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX7.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX8.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX9.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX10.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX11.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX12.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX13.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX14.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX15.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX16.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX17.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX18.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX19.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX20.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX21.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX22.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX23.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX24.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX25.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX26.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX27.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX28.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX29.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX30.MSGCTRL.all = 0x00000000;
    ECanaMboxes.MBOX31.MSGCTRL.all = 0x00000000;

// TAn, RMPn, GIFn bits are all zero upon reset and are cleared again
//	as a matter of precaution.

	ECanaRegs.CANTA.all	= 0xFFFFFFFF;	/* Clear all TAn bits */

	ECanaRegs.CANRMP.all = 0xFFFFFFFF;	/* Clear all RMPn bits */

	ECanaRegs.CANGIF0.all = 0xFFFFFFFF;	/* Clear all interrupt flag bits */
	ECanaRegs.CANGIF1.all = 0xFFFFFFFF;


/* Configure bit timing parameters for eCANA*/
//	ECanaShadow.CANMC.all = ECanaRegs.CANMC.all;
//	ECanaShadow.CANMC.bit.CCR = 1 ;            // Set CCR = 1
//    ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;

 //   ECanaShadow.CANES.all = ECanaRegs.CANES.all;

/* Configure bit timing parameters for eCANA*/

    ECanaShadow.CANMC.all = ECanaRegs.CANMC.all;
    ECanaShadow.CANMC.bit.CCR = 1 ;            // Set CCR = 1
	ECanaShadow.CANMC.bit.DBO = 0;
	ECanaShadow.CANMC.bit.ABO = 1;
	ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;


    do
	{
	    ECanaShadow.CANES.all = ECanaRegs.CANES.all;
    } while(ECanaShadow.CANES.bit.CCE != 1 );  		// Wait for CCE bit to be set..

    ECanaShadow.CANBTC.all = 0;

    #if (CPU_FRQ_150MHZ)                       // CPU_FRQ_150MHz is defined in DSP2833x_Examples.h
		/* The following block for all 150 MHz SYSCLKOUT (75 MHz CAN clock) - default. Bit rate = 1 Mbps
		   See Note at End of File */
			ECanaShadow.CANBTC.bit.BRPREG = 4;
			ECanaShadow.CANBTC.bit.TSEG2REG = 2;
			ECanaShadow.CANBTC.bit.TSEG1REG = 10;

        /* The following block for all 150 MHz SYSCLKOUT (75 MHz CAN clock) - default. Bit rate =500 kbps
           See Note at End of File */
        //    ECanaShadow.CANBTC.bit.BRPREG = 9;
        //    ECanaShadow.CANBTC.bit.TSEG2REG = 2;
        //    ECanaShadow.CANBTC.bit.TSEG1REG = 10;

            
    #endif
	#if (CPU_FRQ_100MHZ)                       // CPU_FRQ_100MHz is defined in DSP2833x_Examples.h
	/* The following block is only for 100 MHz SYSCLKOUT (50 MHz CAN clock). Bit rate = 1 Mbps
	   See Note at End of File */
	    ECanaShadow.CANBTC.bit.BRPREG = 4;
		ECanaShadow.CANBTC.bit.TSEG2REG = 1;
		ECanaShadow.CANBTC.bit.TSEG1REG = 6;
	#endif


	ECanaShadow.CANBTC.bit.SAM = 1;
	ECanaRegs.CANBTC.all = ECanaShadow.CANBTC.all;

	ECanaShadow.CANMC.all = ECanaRegs.CANMC.all;
	ECanaShadow.CANMC.bit.CCR = 0 ;            // Set CCR = 0
	ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;

//	ECanaShadow.CANES.all = ECanaRegs.CANES.all;

    do
    {
       ECanaShadow.CANES.all = ECanaRegs.CANES.all;
    } while(ECanaShadow.CANES.bit.CCE != 0 ); 		// Wait for CCE bit to be  cleared..


// Original code
/* Disable all Mailboxes  */
//	ECanaRegs.CANME.all = 0;		// Required before writing the MSGIDs


// new code
	//_____________________________________________________________________//
	// eCAN interrupt configuration
	// MIM(Mailbox Interrupt Mask), MIL(Mailbox Interrupt Level), GIM(Global Interrupt Mask)
	//_____________________________________________________________________//
	// eCAN Mailbox Interrupt Mask register (CANMIM) bit definitions
	//struct  CANMIM_BITS { 	 // bit  description
	//	 Uint16 	 MIM0:1;	 	// 0		MIM for Mailbox 0
	//	 Uint16 	 MIM1:1;	 	// 1		MIM for Mailbox 1
	//	 Uint16 	 MIM2:1;	 	// 2		MIM for Mailbox 2
	//	 Uint16 	 MIM3:1;	 	// 3		MIM for Mailbox 3
	//	 Uint16 	 MIM4:1;	 	// 4		MIM for Mailbox 4
	//	 Uint16 	 MIM5:1;	 	// 5		MIM for Mailbox 5
	//	 Uint16 	 MIM6:1;	 	// 6		MIM for Mailbox 6
	//	 Uint16 	 MIM7:1;	 	// 7		MIM for Mailbox 7
	//	 Uint16 	 MIM8:1;	 	// 8		MIM for Mailbox 8
	//	 Uint16 	 MIM9:1;		 // 9	MIM for Mailbox 9
	//	 Uint16 	 MIM10:1;	 // 10	MIM for Mailbox 10
	//	 Uint16 	 MIM11:1;	 // 11	MIM for Mailbox 11
	//	 Uint16 	 MIM12:1;	 // 12	MIM for Mailbox 12
	//	 Uint16 	 MIM13:1;	 // 13	MIM for Mailbox 13
	//	 Uint16 	 MIM14:1;	 // 14	MIM for Mailbox 14
	//	 Uint16 	 MIM15:1;	 // 15	MIM for Mailbox 15
	//	 Uint16 	 MIM16:1;	 // 16	MIM for Mailbox 16
	//	 Uint16 	 MIM17:1;	 // 17	MIM for Mailbox 17
	//	 Uint16 	 MIM18:1;	 // 18	MIM for Mailbox 18
	//	 Uint16 	 MIM19:1;	 // 19	MIM for Mailbox 19
	//	 Uint16 	 MIM20:1;	 // 20	MIM for Mailbox 20
	//	 Uint16 	 MIM21:1;	 // 21	MIM for Mailbox 21
	//	 Uint16 	 MIM22:1;	 // 22	MIM for Mailbox 22
	//	 Uint16 	 MIM23:1;	 // 23	MIM for Mailbox 23
	//	 Uint16 	 MIM24:1;	 // 24	MIM for Mailbox 24
	//	 Uint16 	 MIM25:1;	 // 25	MIM for Mailbox 25
	//	 Uint16 	 MIM26:1;	 // 26	MIM for Mailbox 26		<<
	//	 Uint16 	 MIM27:1;	 // 27	MIM for Mailbox 27		<<
	//	 Uint16 	 MIM28:1;	 // 28	MIM for Mailbox 28		<<
	//	 Uint16 	 MIM29:1;	 // 29	MIM for Mailbox 29
	//	 Uint16 	 MIM30:1;	 // 30	MIM for Mailbox 30
	//	 Uint16 	 MIM31:1;	 // 31	MIM for Mailbox 31
	//};
	//_____________________________________________________________________//
	//	7		6		5		4		3		2		1		0
	//	0x80	0x40	0x20	0x10	0x08	0x04	0x02	0x01
	//ECanaRegs.CANMIM.all = 0x0000003C;		//enable interrupt for mailbox<2,3,4,5>
	ECanaRegs.CANMIM.all = 0x000000F0;		//enable interrupt for mailbox<0,1,2,3,4,5,6,7>

	ECanaRegs.CANMIL.all=0x0;
	ECanaRegs.CANGIM.bit.I0EN=1;			//enable ECAN0INT

/* Disable all Mailboxes  */
    ECanaRegs.CANME.all = 0;        // Required before writing the MSGIDs

    EDIS;
}


#if (DSP28_ECANB)
void InitECanb(void)		// Initialize eCAN-B module
{
/* Create a shadow register structure for the CAN control registers. This is
 needed, since only 32-bit access is allowed to these registers. 16-bit access
 to these registers could potentially corrupt the register contents or return
 false data. This is especially true while writing to/reading from a bit
 (or group of bits) among bits 16 - 31 */

struct ECAN_REGS ECanbShadow;

   EALLOW;		// EALLOW enables access to protected bits

/* Configure eCAN RX and TX pins for CAN operation using eCAN regs*/

    ECanbShadow.CANTIOC.all = ECanbRegs.CANTIOC.all;
    ECanbShadow.CANTIOC.bit.TXFUNC = 1;
    ECanbRegs.CANTIOC.all = ECanbShadow.CANTIOC.all;

    ECanbShadow.CANRIOC.all = ECanbRegs.CANRIOC.all;
    ECanbShadow.CANRIOC.bit.RXFUNC = 1;
    ECanbRegs.CANRIOC.all = ECanbShadow.CANRIOC.all;

/* Configure eCAN for HECC mode - (reqd to access mailboxes 16 thru 31) */

	ECanbShadow.CANMC.all = ECanbRegs.CANMC.all;
	ECanbShadow.CANMC.bit.SCB = 1;
	ECanbRegs.CANMC.all = ECanbShadow.CANMC.all;

/* Initialize all bits of 'Master Control Field' to zero */
// Some bits of MSGCTRL register come up in an unknown state. For proper operation,
// all bits (including reserved bits) of MSGCTRL must be initialized to zero

    ECanbMboxes.MBOX0.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX1.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX2.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX3.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX4.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX5.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX6.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX7.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX8.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX9.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX10.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX11.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX12.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX13.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX14.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX15.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX16.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX17.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX18.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX19.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX20.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX21.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX22.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX23.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX24.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX25.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX26.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX27.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX28.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX29.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX30.MSGCTRL.all = 0x00000000;
    ECanbMboxes.MBOX31.MSGCTRL.all = 0x00000000;

// TAn, RMPn, GIFn bits are all zero upon reset and are cleared again
//	as a matter of precaution.

	ECanbRegs.CANTA.all	= 0xFFFFFFFF;	/* Clear all TAn bits */

	ECanbRegs.CANRMP.all = 0xFFFFFFFF;	/* Clear all RMPn bits */

	ECanbRegs.CANGIF0.all = 0xFFFFFFFF;	/* Clear all interrupt flag bits */
	ECanbRegs.CANGIF1.all = 0xFFFFFFFF;


/* Configure bit timing parameters for eCANB*/

	ECanbShadow.CANMC.all = ECanbRegs.CANMC.all;
	ECanbShadow.CANMC.bit.CCR = 1 ;            // Set CCR = 1
    ECanbRegs.CANMC.all = ECanbShadow.CANMC.all;

    ECanbShadow.CANES.all = ECanbRegs.CANES.all;

    do
	{
	    ECanbShadow.CANES.all = ECanbRegs.CANES.all;
	} while(ECanbShadow.CANES.bit.CCE != 1 ); 		// Wait for CCE bit to be  cleared..


    ECanbShadow.CANBTC.all = 0;

    #if (CPU_FRQ_150MHZ)                       // CPU_FRQ_150MHz is defined in DSP2833x_Examples.h
	/* The following block for all 150 MHz SYSCLKOUT (75 MHz CAN clock) - default. Bit rate = 1 Mbps
	   See Note at end of file */
		ECanbShadow.CANBTC.bit.BRPREG = 4;
		ECanbShadow.CANBTC.bit.TSEG2REG = 2;
		ECanbShadow.CANBTC.bit.TSEG1REG = 10;
        
	/* The following block for all 150 MHz SYSCLKOUT (75 MHz CAN clock) - default. Bit rate = 500 kbps
	   See Note at end of file */
		//ECanbShadow.CANBTC.bit.BRPREG = 9;
		//ECanbShadow.CANBTC.bit.TSEG2REG = 2;
		//ECanbShadow.CANBTC.bit.TSEG1REG = 10;
	#endif
	#if (CPU_FRQ_100MHZ)                       // CPU_FRQ_100MHz is defined in DSP2833x_Examples.h
	/* The following block is only for 100 MHz SYSCLKOUT (50 MHz CAN clock). Bit rate = 1 Mbps
	   See Note at end of file */
	    ECanbShadow.CANBTC.bit.BRPREG = 4;
		ECanbShadow.CANBTC.bit.TSEG2REG = 1;
		ECanbShadow.CANBTC.bit.TSEG1REG = 6;
	#endif

    ECanbShadow.CANBTC.bit.SAM = 1;
    ECanbRegs.CANBTC.all = ECanbShadow.CANBTC.all;

    ECanbShadow.CANMC.all = ECanbRegs.CANMC.all;
	ECanbShadow.CANMC.bit.CCR = 0 ;            // Set CCR = 0
    ECanbRegs.CANMC.all = ECanbShadow.CANMC.all;

    ECanbShadow.CANES.all = ECanbRegs.CANES.all;

    do
    {
        ECanbShadow.CANES.all = ECanbRegs.CANES.all;
    } while(ECanbShadow.CANES.bit.CCE != 0 ); 		// Wait for CCE bit to be  cleared..

//_____________________________________________________________________//
	// eCAN interrupt configuration
	// MIM(Mailbox Interrupt Mask), MIL(Mailbox Interrupt Level), GIM(Global Interrupt Mask)
	//_____________________________________________________________________//
	// eCAN Mailbox Interrupt Mask register (CANMIM) bit definitions 
	//struct  CANMIM_BITS { 	 // bit  description
	//	 Uint16 	 MIM0:1;	 	// 0		MIM for Mailbox 0
	//	 Uint16 	 MIM1:1;	 	// 1		MIM for Mailbox 1
	//	 Uint16 	 MIM2:1;	 	// 2		MIM for Mailbox 2
	//	 Uint16 	 MIM3:1;	 	// 3		MIM for Mailbox 3 
	//	 Uint16 	 MIM4:1;	 	// 4		MIM for Mailbox 4 
	//	 Uint16 	 MIM5:1;	 	// 5		MIM for Mailbox 5 
	//	 Uint16 	 MIM6:1;	 	// 6		MIM for Mailbox 6
	//	 Uint16 	 MIM7:1;	 	// 7		MIM for Mailbox 7
	//	 Uint16 	 MIM8:1;	 	// 8		MIM for Mailbox 8
	//	 Uint16 	 MIM9:1;		 // 9	MIM for Mailbox 9
	//	 Uint16 	 MIM10:1;	 // 10	MIM for Mailbox 10
	//	 Uint16 	 MIM11:1;	 // 11	MIM for Mailbox 11
	//	 Uint16 	 MIM12:1;	 // 12	MIM for Mailbox 12
	//	 Uint16 	 MIM13:1;	 // 13	MIM for Mailbox 13
	//	 Uint16 	 MIM14:1;	 // 14	MIM for Mailbox 14
	//	 Uint16 	 MIM15:1;	 // 15	MIM for Mailbox 15
	//	 Uint16 	 MIM16:1;	 // 16	MIM for Mailbox 16
	//	 Uint16 	 MIM17:1;	 // 17	MIM for Mailbox 17
	//	 Uint16 	 MIM18:1;	 // 18	MIM for Mailbox 18
	//	 Uint16 	 MIM19:1;	 // 19	MIM for Mailbox 19
	//	 Uint16 	 MIM20:1;	 // 20	MIM for Mailbox 20
	//	 Uint16 	 MIM21:1;	 // 21	MIM for Mailbox 21
	//	 Uint16 	 MIM22:1;	 // 22	MIM for Mailbox 22
	//	 Uint16 	 MIM23:1;	 // 23	MIM for Mailbox 23
	//	 Uint16 	 MIM24:1;	 // 24	MIM for Mailbox 24
	//	 Uint16 	 MIM25:1;	 // 25	MIM for Mailbox 25
	//	 Uint16 	 MIM26:1;	 // 26	MIM for Mailbox 26		<<
	//	 Uint16 	 MIM27:1;	 // 27	MIM for Mailbox 27		<<
	//	 Uint16 	 MIM28:1;	 // 28	MIM for Mailbox 28		<<
	//	 Uint16 	 MIM29:1;	 // 29	MIM for Mailbox 29
	//	 Uint16 	 MIM30:1;	 // 30	MIM for Mailbox 30
	//	 Uint16 	 MIM31:1;	 // 31	MIM for Mailbox 31
	//};	
	//_____________________________________________________________________//
	//	7		6		5		4		3		2		1		0
	//	0x80	0x40	0x20	0x10	0x08	0x04	0x02	0x01
	//ECanbRegs.CANMIM.all = 0x0000003C;		//enable interrupt for mailbox<2,3,4,5>
	ECanbRegs.CANMIM.all = 0x000000F0;		//enable interrupt for mailbox<0,1,2,3,4,5,6,7>

	ECanbRegs.CANMIL.all=0x0;
	ECanbRegs.CANGIM.bit.I0EN=1;				//enable ECAN1INT


/* Disable all Mailboxes  */
 	ECanbRegs.CANME.all = 0;		// Required before writing the MSGIDs





    EDIS;
}
#endif // if DSP28_ECANB


//---------------------------------------------------------------------------
// Example: InitECanGpio:
//---------------------------------------------------------------------------
// This function initializes GPIO pins to function as eCAN pins
//
// Each GPIO pin can be configured as a GPIO pin or up to 3 different
// peripheral functional pins. By default all pins come up as GPIO
// inputs after reset.
//
// Caution:
// Only one GPIO pin should be enabled for CANTXA/B operation.
// Only one GPIO pin shoudl be enabled for CANRXA/B operation.
// Comment out other unwanted lines.


void InitECanGpio(void)
{
#if (DSP28_ECANA)
   InitECanaGpio();
#endif // if DSP28_ECANA

#if (DSP28_ECANB)
   InitECanbGpio();
#endif // if DSP28_ECANB
}

void InitECanaGpio(void)	// SP6 MCB
{
   EALLOW;

/* Enable internal pull-up for the selected CAN pins */
// Pull-ups can be enabled or disabled by the user.
// This will enable the pullups for the specified pins.
// Comment out other unwanted lines.

	GpioCtrlRegs.GPAPUD.bit.GPIO30 = 0;	    // Enable pull-up for GPIO30 (CANRXA)
//	GpioCtrlRegs.GPAPUD.bit.GPIO18 = 0;	    // Enable pull-up for GPIO18 (CANRXA)

	GpioCtrlRegs.GPAPUD.bit.GPIO31 = 0;	    // Enable pull-up for GPIO31 (CANTXA)
//	GpioCtrlRegs.GPAPUD.bit.GPIO19 = 0;	    // Enable pull-up for GPIO19 (CANTXA)

/* Set qualification for selected CAN pins to asynch only */
// Inputs are synchronized to SYSCLKOUT by default.
// This will select asynch (no qualification) for the selected pins.

   GpioCtrlRegs.GPAQSEL2.bit.GPIO30 = 3;   // Asynch qual for GPIO30 (CANRXA)
//  GpioCtrlRegs.GPAQSEL2.bit.GPIO18 = 3;   // Asynch qual for GPIO18 (CANRXA)


/* Configure eCAN-A pins using GPIO regs*/
// This specifies which of the possible GPIO pins will be eCAN functional pins.

	GpioCtrlRegs.GPAMUX2.bit.GPIO30 = 1;	// Configure GPIO30 for CANRXA operation
//  GpioCtrlRegs.GPAMUX2.bit.GPIO18 = 3;	// Configure GPIO18 for CANRXA operation
	GpioCtrlRegs.GPAMUX2.bit.GPIO31 = 1;	// Configure GPIO31 for CANTXA operation
//  GpioCtrlRegs.GPAMUX2.bit.GPIO19 = 3;	// Configure GPIO19 for CANTXA operation

    EDIS;
}

#if (DSP28_ECANB)
void InitECanbGpio(void)
{
   EALLOW;

/* Enable internal pull-up for the selected CAN pins */
// Pull-ups can be enabled or disabled by the user.
// This will enable the pullups for the specified pins.
// Comment out other unwanted lines.

//	GpioCtrlRegs.GPAPUD.bit.GPIO8 = 0;	  // Enable pull-up for GPIO8  (CANTXB)
//  GpioCtrlRegs.GPAPUD.bit.GPIO12 = 0;   // Enable pull-up for GPIO12 (CANTXB)
  GpioCtrlRegs.GPAPUD.bit.GPIO16 = 0;   // Enable pull-up for GPIO16 (CANTXB)
//  GpioCtrlRegs.GPAPUD.bit.GPIO20 = 0;   // Enable pull-up for GPIO20 (CANTXB)

//	GpioCtrlRegs.GPAPUD.bit.GPIO10 = 0;	  // Enable pull-up for GPIO10 (CANRXB)
//  GpioCtrlRegs.GPAPUD.bit.GPIO13 = 0;   // Enable pull-up for GPIO13 (CANRXB)
  GpioCtrlRegs.GPAPUD.bit.GPIO17 = 0;   // Enable pull-up for GPIO17 (CANRXB)
//  GpioCtrlRegs.GPAPUD.bit.GPIO21 = 0;   // Enable pull-up for GPIO21 (CANRXB)

/* Set qualification for selected CAN pins to asynch only */
// Inputs are synchronized to SYSCLKOUT by default.
// This will select asynch (no qualification) for the selected pins.
// Comment out other unwanted lines.

//    GpioCtrlRegs.GPAQSEL1.bit.GPIO10 = 3; // Asynch qual for GPIO10 (CANRXB)
//  GpioCtrlRegs.GPAQSEL1.bit.GPIO13 = 3; // Asynch qual for GPIO13 (CANRXB)
  GpioCtrlRegs.GPAQSEL2.bit.GPIO17 = 3; // Asynch qual for GPIO17 (CANRXB)
//  GpioCtrlRegs.GPAQSEL2.bit.GPIO21 = 3; // Asynch qual for GPIO21 (CANRXB)

/* Configure eCAN-B pins using GPIO regs*/
// This specifies which of the possible GPIO pins will be eCAN functional pins.

//	GpioCtrlRegs.GPAMUX1.bit.GPIO8 = 2;   // Configure GPIO8 for CANTXB operation
//  GpioCtrlRegs.GPAMUX1.bit.GPIO12 = 2;  // Configure GPIO12 for CANTXB operation
  GpioCtrlRegs.GPAMUX2.bit.GPIO16 = 2;  // Configure GPIO16 for CANTXB operation
//  GpioCtrlRegs.GPAMUX2.bit.GPIO20 = 3;  // Configure GPIO20 for CANTXB operation

//	GpioCtrlRegs.GPAMUX1.bit.GPIO10 = 2;  // Configure GPIO10 for CANRXB operation
//  GpioCtrlRegs.GPAMUX1.bit.GPIO13 = 2;  // Configure GPIO13 for CANRXB operation
  GpioCtrlRegs.GPAMUX2.bit.GPIO17 = 2;  // Configure GPIO17 for CANRXB operation
//  GpioCtrlRegs.GPAMUX2.bit.GPIO21 = 3;  // Configure GPIO21 for CANRXB operation

    EDIS;
}
#endif // if DSP28_ECANB

/*
Note: Bit timing parameters must be chosen based on the network parameters such
as the sampling point desired and the propagation delay of the network.
The propagation delay is a function of length of the cable, delay introduced by
the transceivers and opto/galvanic-isolators (if any).

The parameters used in this file must be changed taking into account the above
mentioned factors in order to arrive at the bit-timing parameters suitable
for a network.

*/
