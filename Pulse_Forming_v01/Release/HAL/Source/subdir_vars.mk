################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../HAL/Source/HAL_ADC.c \
../HAL/Source/HAL_EncorderSW.c \
../HAL/Source/HAL_InputSW.c \
../HAL/Source/HAL_LED.c \
../HAL/Source/HAL_XD1602.c \
../HAL/Source/HAL_XINT.c \
../HAL/Source/HAL_ePWM.c 

C_DEPS += \
./HAL/Source/HAL_ADC.d \
./HAL/Source/HAL_EncorderSW.d \
./HAL/Source/HAL_InputSW.d \
./HAL/Source/HAL_LED.d \
./HAL/Source/HAL_XD1602.d \
./HAL/Source/HAL_XINT.d \
./HAL/Source/HAL_ePWM.d 

OBJS += \
./HAL/Source/HAL_ADC.obj \
./HAL/Source/HAL_EncorderSW.obj \
./HAL/Source/HAL_InputSW.obj \
./HAL/Source/HAL_LED.obj \
./HAL/Source/HAL_XD1602.obj \
./HAL/Source/HAL_XINT.obj \
./HAL/Source/HAL_ePWM.obj 

OBJS__QUOTED += \
"HAL\Source\HAL_ADC.obj" \
"HAL\Source\HAL_EncorderSW.obj" \
"HAL\Source\HAL_InputSW.obj" \
"HAL\Source\HAL_LED.obj" \
"HAL\Source\HAL_XD1602.obj" \
"HAL\Source\HAL_XINT.obj" \
"HAL\Source\HAL_ePWM.obj" 

C_DEPS__QUOTED += \
"HAL\Source\HAL_ADC.d" \
"HAL\Source\HAL_EncorderSW.d" \
"HAL\Source\HAL_InputSW.d" \
"HAL\Source\HAL_LED.d" \
"HAL\Source\HAL_XD1602.d" \
"HAL\Source\HAL_XINT.d" \
"HAL\Source\HAL_ePWM.d" 

C_SRCS__QUOTED += \
"../HAL/Source/HAL_ADC.c" \
"../HAL/Source/HAL_EncorderSW.c" \
"../HAL/Source/HAL_InputSW.c" \
"../HAL/Source/HAL_LED.c" \
"../HAL/Source/HAL_XD1602.c" \
"../HAL/Source/HAL_XINT.c" \
"../HAL/Source/HAL_ePWM.c" 


