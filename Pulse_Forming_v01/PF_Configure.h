/*
 * PF_Configure.h
 *
 *  Created on: 2016. 2. 26.
 *      Author: gisu
 */

#ifndef PF_CONFIGURE_H_
#define PF_CONFIGURE_H_
#define TRUE 1
#define FALSE 0

#define DEBUG			FALSE
#define RUN_ON_FLASH 	1
#define RUN_ON_RAM		0

#define RUN_ON_TYPE 	RUN_ON_FLASH


#include "DSP2833x_Device.h"     	// DSP2833x Headerfile Include File
#include "DSP2833x_Examples.h"   	// DSP2833x Examples Include File



typedef enum State{
	IdleMode = 0,
	RunningMode,
	ConfigMode,
	ReadyMode,
	Triggered
} DeviceStatus;
//
//typedef enum ConfigTypeSel{
//	TestTypeSelect=0,
//	PeriodeSelect,
//	FrequncySeclect
//
//} CfgType;

typedef enum TestTypeSel{
	NotSelect =0,
	VoltageMode,
	CurrentMode,
	didtRepeatMode,
	didtNonRepeatMode,
	ITSMMode,
	I2TMode,
	error

} SelectTest, SelectConfig;
//
// CfgType ConfigType;

// SelectTest TestTypeSel;

#endif /* PF_CONFIGURE_H_ */
