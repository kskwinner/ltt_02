
#include "HAL_ePWM.h"

#define	SYSTEM_CLOCK	150E6	/* 150MHz */
#define	TBCLK			3125000	/* 150MHz */
#define	PWM_CARRIER		60	/* 20kHz */
#define	PWM_DUTY_RATIO	2E-1	/* 0.2, 20% */


#define	UFEC	0	/* Up-Count, Falling Edge Control */
#define	UREC	0	/* Up-Count, Rising Edge Control */
#define	DFEC	0	/* Down-Count, Falling Edge Control */
#define	DREC	1	/* Down-Count, Rising Edge Control */

void InitEPWM1GPIO(void)
{
	EALLOW;
//	GpioCtrlRegs.GPAPUD.bit.GPIO0 = 0;	/* Enable pull-up on GPIO0 (EPWM1A) */
//	GpioCtrlRegs.GPAPUD.bit.GPIO1 = 0;	/* Enable pull-up on GPIO1 (EPWM1B) */
//    GpioCtrlRegs.GPAMUX1.bit.GPIO0 = 1;	/* Configure GPIO0 as EPWM1A */
//	GpioCtrlRegs.GPAMUX1.bit.GPIO1 = 1;	/* Configure GPIO1 as EPWM1B */
    EDIS;
}

void InitEPwm1Module(void)
{

//DeadBand ����
//**********************************************************************************************
/* Setup Counter Mode and Clock */
//	EPwm1Regs.TBCTL.bit.CTRMODE = 0;		/* Count Up (Asymmetric) */
//	EPwm1Regs.TBCTL.bit.HSPCLKDIV = 0;		/* TBCLK = SYSCLKOUT / (HSPCLKDIV * CLKDIV) = 150MHz */
//	EPwm1Regs.TBCTL.bit.CLKDIV = 0;
//
//	/* Setup Phase */
//	EPwm1Regs.TBPHS.half.TBPHS = 0;			/* Phase is 0 */
//	EPwm1Regs.TBCTL.bit.PHSEN = 0;			/* Disable phase loading */
//
//	/* Setup Period (Carrier Frequency) */
//	EPwm1Regs.TBPRD = (TBCLK/PWM_CARRIER)-1;	/* Set Timer Period, (150MHz/100KHz)-1 = 1,499 (0x05DB) */
//	EPwm1Regs.TBCTR = 0;						/* Clear Counter */
//
//	/* Set Compare Value */
//	EPwm1Regs.CMPA.half.CMPA = (Uint16)((EPwm1Regs.TBPRD + 1) * PWM_DUTY_RATIO_A);	/* Set Compare A Value to 50% */
//
//	/* Setup shadowing */
//	EPwm1Regs.TBCTL.bit.PRDLD = 0;			/* Period Register is loaded from its shadow when CNTR=Zero */
//	EPwm1Regs.CMPCTL.bit.SHDWAMODE = 0;		/* Compare A Register is loaded from its shadow when CNTR=Zero */
//	EPwm1Regs.CMPCTL.bit.LOADAMODE = 0;
//
//	/* Set actions */
//	EPwm1Regs.AQCTLA.bit.ZRO = 2;			/* Set EPWM4A on CNTR=Zero */
//	EPwm1Regs.AQCTLA.bit.CAU = 1;			/* Clear EPWM4A on CNTR=CMPA, Up-Count */
//
//	/* Set Dead-time */
//	EPwm1Regs.DBCTL.bit.IN_MODE = 0;		/* EPWMxA is the source for both falling-edge & rising-edge delay */
//	EPwm1Regs.DBCTL.bit.OUT_MODE = 3;		/* Dead-band is fully enabled for both rising-edge delay on EPWMxA and falling-edge delay on EPWMxB */
//	EPwm1Regs.DBCTL.bit.POLSEL = 2;			/* Active High Complementary (AHC). EPWMxB is inverted */
//	EPwm1Regs.DBFED = 75;					/* 2usec, Falling Edge Delay */
//	EPwm1Regs.DBRED = 75;					/* 1usec, Rising Edge Delay */
//
//	/* Set Interrupts */
//	EPwm1Regs.ETSEL.bit.INTSEL = 1;		/* Select INT on CNTR=Zero */
//	EPwm1Regs.ETPS.bit.INTPRD = 1;		/* Generate INT on 1st event */
//	EPwm1Regs.ETSEL.bit.INTEN = 1;		/* Enable INT */

	//**********************************************************************************************



	/* Setup Counter Mode and Clock */
	#if(UFEC)
		EPwm1Regs.TBCTL.bit.CTRMODE = 0;	/* Count Up (Asymmetric) */
	#endif

	#if(UREC)
		EPwm1Regs.TBCTL.bit.CTRMODE = 0;	/* Count Up (Asymmetric) */
	#endif

	#if(DFEC)
		EPwm1Regs.TBCTL.bit.CTRMODE = 1;	/* Count Down (Asymmetric) */
	#endif

	#if(DREC)
		EPwm1Regs.TBCTL.bit.CTRMODE = 1;	/* Count Down (Asymmetric) */
	#endif

	EPwm1Regs.TBCTL.bit.HSPCLKDIV = 3;		/* TBCLK = SYSCLKOUT / (HSPCLKDIV * CLKDIV) = 150MHz */
	EPwm1Regs.TBCTL.bit.CLKDIV = 3;
	/* Setup Phase */
	EPwm1Regs.TBPHS.half.TBPHS = 0;			/* Phase is 0 */
	EPwm1Regs.TBCTL.bit.PHSEN = 0;			/* Disable phase loading */

	/* Setup Period (Carrier Frequency) */
	EPwm1Regs.TBPRD = (TBCLK/PWM_CARRIER)-1;	/* Set Timer Period, (150MHz/20KHz)-1 = 7,499 (0x1D4B) */
	EPwm1Regs.TBCTR = 0;						/* Clear Counter */

	/* Set Compare Value */
	#if(UFEC)
		/* Set Compare A Value to 20% */
		EPwm1Regs.CMPA.half.CMPA = (Uint16)((EPwm1Regs.TBPRD + 1) * PWM_DUTY_RATIO);
	#endif

	#if(UREC)
		/* Set Compare A Value to 20% */
		EPwm1Regs.CMPA.half.CMPA = (Uint16)(EPwm1Regs.TBPRD - ((EPwm1Regs.TBPRD + 1) * PWM_DUTY_RATIO));
	#endif

	#if(DFEC)
		/* Set Compare A Value to 20% */
		EPwm1Regs.CMPA.half.CMPA = (Uint16)(EPwm1Regs.TBPRD - ((EPwm1Regs.TBPRD + 1) * PWM_DUTY_RATIO));
	#endif

	#if(DREC)
		/* Set Compare A Value to 20% */
		EPwm1Regs.CMPA.half.CMPA = (Uint16)((EPwm1Regs.TBPRD + 1) * PWM_DUTY_RATIO);
	#endif

	/* Setup shadowing */
	EPwm1Regs.TBCTL.bit.PRDLD = 0;			/* Period Register is loaded from its shadow when CNTR=Zero */
	EPwm1Regs.CMPCTL.bit.SHDWAMODE = 0;		/* Compare A Register is loaded from its shadow when CNTR=Zero */
	EPwm1Regs.CMPCTL.bit.LOADAMODE = 0;

	/* Set actions */
	#if(UFEC)
		EPwm1Regs.AQCTLA.bit.ZRO = 2;		/* Set EPWM4A on CNTR=Zero */
		EPwm1Regs.AQCTLA.bit.CAU = 1;		/* Clear EPWM4A on CNTR=CMPA, Up-Count */
	#endif

	#if(UREC)
		EPwm1Regs.AQCTLA.bit.CAU = 2;		/* Set EPWM4A on CNTR=CMPA, Up-Count */
		EPwm1Regs.AQCTLA.bit.PRD = 1;		/* Clear EPWM4A on CNTR=PRD */
	#endif

	#if(DFEC)
		EPwm1Regs.AQCTLA.bit.PRD = 2;		/* Set EPWM4A on CNTR=PRD */
		EPwm1Regs.AQCTLA.bit.CAD = 1;		/* Clear EPWM4A on CNTR=CMPA, Down-Count */
	#endif

	#if(DREC)
		EPwm1Regs.AQCTLA.bit.CAD = 2;		/* Set EPWM4A on CNTR=CMPA, Down-Count */
		EPwm1Regs.AQCTLA.bit.ZRO = 1;		/* Clear EPWM4A on CNTR=Zero */
//		EPwm1Regs.AQCTLA.bit.CAD = 0;		/* Set EPWM4A on CNTR=CMPA, Down-Count */
//		EPwm1Regs.AQCTLA.bit.ZRO = 0;		/* Clear EPWM4A on CNTR=Zero */
	#endif

		EPwm1Regs.ETSEL.bit.INTSEL = 2;
		EPwm1Regs.ETSEL.bit.SOCASEL = 2;
		EPwm1Regs.ETPS.bit.INTPRD = 1;
		EPwm1Regs.ETPS.bit.SOCAPRD = 1;
		EPwm1Regs.ETSEL.bit.INTEN = 1;
		EPwm1Regs.ETSEL.bit.SOCAEN = 1;


}
//deadband
//*********************************************************************
//	BackTicker = 0;
//	EPwm1IsrTicker = 0;
//
//	PwmCarrierFrequency = PWM_CARRIER;
//	PwmDutyRatioA = PWM_DUTY_RATIO_A;
//	FallingEdgeDelay = (1.0 / TBCLK) * EPwm1Regs.DBFED;
//	RisingEdgeDelay = (1.0 / TBCLK) * EPwm1Regs.DBRED;
//*********************************************************************
//	int isIdleMode = FALSE;
