/*
 * HAL_ADC.c
 *
 *  Created on: 2016. 7. 15.
 *      Author: gisu
 */
#include "HAL_ADC.h"

void ADC_GPIO_INIT()
{
	EALLOW;
//    GpioCtrlRegs.GPAPUD.bit.GPIO1 = 0;  // Enable pullup on GPIO1
//    GpioCtrlRegs.GPAMUX1.bit.GPIO1 = 0; // GPIO1 = GPIO1
//    GpioCtrlRegs.GPADIR.bit.GPIO1 = 1;  // GPIO1 = input
//    GpioDataRegs.GPASET.bit.GPIO1 = 0;   // Load output latch

//    GpioCtrlRegs.GPAPUD.bit.GPIO2 = 0;  // Enable pullup on GPIO1
//    GpioCtrlRegs.GPAMUX1.bit.GPIO2 = 0; // GPIO1 = GPIO1
//    GpioCtrlRegs.GPADIR.bit.GPIO2 = 1;  // GPIO1 = input
//    GpioDataRegs.GPASET.bit.GPIO2 = 0;   // Load output latch

//    GpioCtrlRegs.GPAPUD.bit.GPIO3 = 0;  // Enable pullup on GPIO1
//    GpioCtrlRegs.GPAMUX1.bit.GPIO3 = 0; // GPIO1 = GPIO1
//    GpioCtrlRegs.GPADIR.bit.GPIO3 = 1;  // GPIO1 = input
//    GpioDataRegs.GPASET.bit.GPIO3 = 0;   // Load output latch
	EDIS;
}
void ConfigADC()
{

	//poling
//	AdcRegs.ADCTRL3.bit.ADCCLKPS = 3;	   		// ADCCLK = HSPCLK/(ADCCLKPS*2)/(CPS+1)
//	AdcRegs.ADCTRL1.bit.CPS = 1;				// ADCCLK = 75MHz/(3*2)/(1+1) = 6.25MHz
//	AdcRegs.ADCTRL1.bit.ACQ_PS = 3;				// 샘플/홀드 사이클 = ACQ_PS + 1 = 4 (ADCCLK기준)
//	AdcRegs.ADCTRL1.bit.SEQ_CASC = 1;			// 시퀀스 모드 설정: 직렬 시퀀스 모드 (0:병렬 모드, 1:직렬 모드)
//	AdcRegs.ADCMAXCONV.bit.MAX_CONV1 = 0;   	// ADC 채널수 설정: 1개(=MAX_CONV+1)채널을 ADC
//	AdcRegs.ADCCHSELSEQ1.bit.CONV00 = 0; 		// ADC 순서 설정: 첫번째로 ADCINA0 채널을 ADC


	//Interrupt ADC 설정
	AdcRegs.ADCTRL3.bit.ADCCLKPS = 3;	   		// ADCCLK = HSPCLK/(ADCCLKPS*2)/(CPS+1)
	AdcRegs.ADCTRL1.bit.CPS = 1;				// ADCCLK = 75MHz/(3*2)/(1+1) = 6.25MHz
	AdcRegs.ADCTRL1.bit.ACQ_PS = 3;				// 샘플/홀드 사이클 = ACQ_PS + 1 = 4 (ADCCLK기준)
	AdcRegs.ADCTRL1.bit.SEQ_CASC = 1;			// 시퀀스 모드 설정: 직렬 시퀀스 모드 (0:병렬 모드, 1:직렬 모드)
	AdcRegs.ADCMAXCONV.bit.MAX_CONV1 = 0;		// ADC 채널수 설정: 1개(=MAX_CONV+1)채널을 ADC
	AdcRegs.ADCCHSELSEQ1.bit.CONV00 = 0; 		// ADC 순서 설정: 첫번째로 ADCINA2 채널을 ADC

	AdcRegs.ADCTRL2.bit.EPWM_SOCB_SEQ = 1; 		// ePWM_SOCB로 ADC 시퀀스 시동
	AdcRegs.ADCTRL2.bit.INT_ENA_SEQ1 = 1;		// ADC 시퀀스 완료시 인터럽트 발생 설정

	//ePWM_SOCB 이벤트 트리거 설정
	EPwm3Regs.ETSEL.bit.SOCBEN = 1;				// SOCB 이벤트 트리거 Enable
	EPwm3Regs.ETSEL.bit.SOCBSEL = 2;			// SCCB 트리거 조건 : 카운터 주기 일치 시
	EPwm3Regs.ETPS.bit.SOCBPRD = 1;				// SOCB 이벤트 분주 설정 : 트리거 조건 한번 마다
	EPwm3Regs.TBCTL.bit.CTRMODE = 0;			// 카운트 모드 설정: Up-conut 모드
	EPwm3Regs.TBCTL.bit.HSPCLKDIV = 1;			// TBCLK = [SYSCLKOUT / ((HSPCLKDIV*2) * 2^(CLKDIV))]
	EPwm3Regs.TBCTL.bit.CLKDIV = 1;				// TBCLK = [150MHz / (2*2)] = 37.5MHz
   	EPwm3Regs.TBPRD = 1874;						// TB주기= (TBPRD+1)/TBCLK = 1875/37.5MHz = 50us(20KHz)
	EPwm3Regs.TBCTR = 0x0000;					// TB 카운터 초기화

}

