
//******************************************************************************
//!
//!
//! This example uses the following peripherals and I/O signals.  You must
//! review these and change as needed for your own board:
//! - UART peripheral
//! - GPIO Port peripheral (for UART pins)
//! - UCA0TXD
//! - UCA0RXD
//!
//! This example uses the following interrupt handlers.  To use this example
//! in your own application you must add these interrupt handlers to your
//! vector table.
//! - USCI_A1_VECTOR.
//******************************************************************************
/***************************************************************************//**
 * @file       SCU_HAL_GPIO.c
 * @addtogroup SCU_HAL_GPIO
 * @{
 ******************************************************************************/

#include "HAL_LED.h" //
#include "DSP2833x_Device.h"     // DSP2833x Headerfile Include File
#include "DSP2833x_Examples.h"   // DSP2833x Examples Include File
//#include "SP5_LSCU_Settings.h"


 void InitLEDGpio(void)
{

   EALLOW;

//        //POW_LED
//        // Make GPIO1 an output
//        GpioCtrlRegs.GPAPUD.bit.GPIO1 = 0;  // Enable pullup on GPIO1
//        GpioCtrlRegs.GPAMUX1.bit.GPIO1 = 0; // GPIO1 = GPIO1
//        GpioCtrlRegs.GPADIR.bit.GPIO1 = 1;  // GPIO1 = input
//        GpioDataRegs.GPASET.bit.GPIO1 = 0;   // Load output latch
//
//
//        //TIMER_CHK_LED
//        // Make GPIO2 an output
//        GpioCtrlRegs.GPAPUD.bit.GPIO2 = 0;  // Enable pullup on GPIO2
//        GpioCtrlRegs.GPAMUX1.bit.GPIO2 = 0; // GPIO2 = GPIO2
//        GpioCtrlRegs.GPADIR.bit.GPIO2 = 1;  // GPIO2 = input
//        GpioDataRegs.GPASET.bit.GPIO2 = 0;   // Load output latch
//
//
//        //EMERGENCY_BREAK_LED
//        // Make GPIO3 an output
//        GpioCtrlRegs.GPAPUD.bit.GPIO3 = 0;  // Enable pullup on GPIO3
//        GpioCtrlRegs.GPAMUX1.bit.GPIO3 = 0; // GPIO3 = GPIO3
//        GpioCtrlRegs.GPADIR.bit.GPIO3 = 1;  // GPIO3 = input
//        GpioDataRegs.GPASET.bit.GPIO3 = 0;   // Load output latch
//
//        //LED5
//        //Make GPIO4 an output
//        GpioCtrlRegs.GPAPUD.bit.GPIO4 = 0;  // Enable pullup on GPIO4
//        GpioCtrlRegs.GPAMUX1.bit.GPIO4 = 0; // GPIO4 = GPIO4
//        GpioCtrlRegs.GPADIR.bit.GPIO4 = 1;  // GPIO4 = input
//        GpioDataRegs.GPASET.bit.GPIO4 = 0;   // Load output latch
//
//        //LED6
//        // Make GPIO5 an output
//        GpioCtrlRegs.GPAPUD.bit.GPIO5 = 0;  // Enable pullup on GPIO2
//        GpioCtrlRegs.GPAMUX1.bit.GPIO5 = 0; // GPIO2 = GPIO2
//        GpioCtrlRegs.GPADIR.bit.GPIO5 = 1;  // GPIO2 = input
//        GpioDataRegs.GPASET.bit.GPIO5 = 0;   // Load output latch
//


   EDIS;
}





