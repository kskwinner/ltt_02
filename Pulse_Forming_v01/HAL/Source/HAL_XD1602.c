//******************************************************************************
//!
//!
//! This example uses the following peripherals and I/O signals.  You must
//! review these and change as needed for your own board:
//! - UART peripheral
//! - GPIO Port peripheral (for UART pins)
//! - UCA0TXD
//! - UCA0RXD
//!
//! This example uses the following interrupt handlers.  To use this example
//! in your own application you must add these interrupt handlers to your
//! vector table.
//! - USCI_A1_VECTOR.
//******************************************************************************
/***************************************************************************//**
 * @file       HAL_RS232.c
 * @addtogroup HAL_RS232
 * @{
 ******************************************************************************/


#include <HAL_XD1602.h>




#define	TMS320F28335CLK		150000000L   

#define	LSPCLK		        (TMS320F28335CLK/4)
#define	BAUDRATE_A	        9600L
#define	BRR_VAL_A		    (LSPCLK/(8*BAUDRATE_A)-1)


void InitXD1602SerialGpio(void)
{
   EALLOW;

/* Enable internal pull-up for the selected pins */
// Pull-ups can be enabled or disabled disabled by the user.
// This will enable the pullups for the specified pins.

	GpioCtrlRegs.GPAPUD.bit.GPIO28 = 0;    // Enable pull-up for GPIO28 (SCIRXDA)
	GpioCtrlRegs.GPAPUD.bit.GPIO29 = 0;	   // Enable pull-up for GPIO29 (SCITXDA)

/* Set qualification for selected pins to asynch only */
// Inputs are synchronized to SYSCLKOUT by default.
// This will select asynch (no qualification) for the selected pins.

	GpioCtrlRegs.GPAQSEL2.bit.GPIO28 = 3;  // Asynch input GPIO28 (SCIRXDA)

/* Configure SCI-A pins using GPIO regs*/
// This specifies which of the possible GPIO pins will be SCI functional pins.

	GpioCtrlRegs.GPAMUX2.bit.GPIO28 = 1;   // Configure GPIO28 for SCIRXDA operation
	GpioCtrlRegs.GPAMUX2.bit.GPIO29 = 1;   // Configure GPIO29 for SCITXDA operation

    EDIS;
}

 // Test 1,SCIA  DLB, 8-bit word, baud rate 0x000F, default, 1 STOP bit, no parity
void InitXD1602Comm(void)
{

	SciaRegs.SCICTL1.bit.SWRESET = 0;		// SCI 소프트웨어 리셋
	SciaRegs.SCICCR.bit.PARITYENA = 0;
	SciaRegs.SCICCR.bit.SCICHAR = 7;		// SCI 송수신 Charcter-length 설정 : 8bit
	SciaRegs.SCICCR.bit.STOPBITS = 0;		// SCI STOPBIT 1
	SciaRegs.SCICCR.bit.LOOPBKENA = 0;		// SCI 루프백 테스트 모드 Disable
	SciaRegs.SCICTL1.bit.RXENA = 1;			// SCI 송신기능 Enable
	SciaRegs.SCICTL1.bit.TXENA = 1;			// SCI 수신기능 Enable
	SciaRegs.SCIHBAUD = BRR_VAL_A >> 8;		    // High Value
	SciaRegs.SCILBAUD = BRR_VAL_A & 0xff;		// Low Value

	SciaRegs.SCICTL1.bit.SWRESET = 1;		// SCI 소프트웨어 리셋 해제
}

void DemoMode(){
	SendByte(0xe9);
	DELAY_US(10000);
}

void TestMSG(){

	ClearLCD();
	DELAY_US(10000);
	SendByte(0xe2);
	SendString("한글은 써지냐?");
	SendByte(0xd5);
	SendByte(0xe0);
	DELAY_US(10000);
}


void SetStringFull(char* str){

	SendByte(CMD_FULL_STX);
	SendString(str);
	SendByte(CMD_FULL_ETX);
	SendByte(CMD_DISPLAY);
	DELAY_US(30000);

}

void SetStringFullAdd(char* str){

	SendByte(CMD_FULL_ADD);
	SendString(str);
	SendByte(CMD_FULL_ETX);
	SendByte(CMD_DISPLAY);
	DELAY_US(30000);

}

void SetStringHarf(char* str){

	SendByte(CMD_HARF_STX);
	SendString(str);
	SendByte(CMD_DISPLAY);
	DELAY_US(10000);

}

void SetStringHarfAdd(char* str){

	SendByte(CMD_HARF_ADD);
	SendString(str);
	SendByte(CMD_DISPLAY);
	DELAY_US(10000);

}

void SetPosition(int x, int y){

	SendByte(CMD_POS_SET);
	SendByte((char)x);
	SendByte((char)y);
	DELAY_US(10000);
}


void InItLCD(){
	SendByte(CMD_INIT);
	DELAY_US(100000);
}

void ClearLCD(){

		SendByte(CMD_LCD_CLEAR);
		DELAY_US(10000);
}
void ClearLCDL1(){

		SendByte(CMD_LCD_L1_CLEAR);
		DELAY_US(10000);
}
void ClearLCDL2(){

		SendByte(CMD_LCD_L2_CLEAR);
		DELAY_US(10000);
}
void CursorOn(){

		SendByte(CMD_C_ON);
		DELAY_US(10000);
}

void CursorBlink(){

		SendByte(0xf3);
		DELAY_US(10000);
}

void CursorReverse(){

		SendByte(CMD_C_REVERSE);
		DELAY_US(10000);
}
// Transmit a character from the SCI
void SendByte(char a)
{
    while (SciaRegs.SCICTL2.bit.TXRDY != 1) {}
    SciaRegs.SCITXBUF=a;

}

void SendString(char * msg)
{
    int i;
    i = 0;
    while(msg[i] != '\0')
    {
    	SendByte(msg[i]);
        i++;
    }
}

void SendInt(long value)
{

	char buffer[10];

	ltoa(value, buffer);
	SetStringHarf(buffer);
	SetStringHarf("");

}

//int  nInt  = 1234567890;
//int  nInt2 = 255;
//long nLong = -1234567890L;
//unsigned long nULong = 345678902UL;
//
//float nFloat = 3.141592f;
//double nDouble = 3.14e+5;
//
//
//char buffer[100];
//int dec, sign;
//char *pbuffer;
//
////10진 문자열로 변환
//
//printf( "%s\n", buffer );
//
//ltoa( nLong, buffer, radix );
//printf( "%s\n", buffer );
//
//itoa( nULong, buffer, radix  );
//printf( "%s\n", buffer );



//===========================================================================
// No more.
//===========================================================================
