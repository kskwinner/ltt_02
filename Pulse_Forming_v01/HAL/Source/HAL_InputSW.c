/*
 * HAL_InputSW.c
 *
 *  Created on: 2016. 9. 9.
 *      Author: gisu
 */


#include "HAL_InputSW.h"


void InitSwitchGPIO()
{
	EALLOW;

    GpioCtrlRegs.GPAPUD.bit.GPIO0 = 0;  // Enable pullup on GPIO0
    GpioCtrlRegs.GPAMUX1.bit.GPIO0 = 0; // GPIO0 = GPIO0
    GpioCtrlRegs.GPADIR.bit.GPIO0 = 0;  // GPIO0 = input
    GpioDataRegs.GPASET.bit.GPIO0 = 0;   // Config

    GpioCtrlRegs.GPAPUD.bit.GPIO14 = 0;  // Enable pullup on GPIO14
    GpioCtrlRegs.GPAMUX1.bit.GPIO14 = 0; // GPIO14 = GPIO
    GpioCtrlRegs.GPADIR.bit.GPIO14 = 0;  // GPIO14 = input
    GpioDataRegs.GPASET.bit.GPIO14 = 0;   // Di/DT 반복

    GpioCtrlRegs.GPAPUD.bit.GPIO15 = 0;  // Enable pullup on GPIO15
    GpioCtrlRegs.GPAMUX1.bit.GPIO15 = 0; // GPIO15 = GPIO
    GpioCtrlRegs.GPADIR.bit.GPIO15 = 0;  // GPIO15 = input
    GpioDataRegs.GPASET.bit.GPIO15 = 0;   // DI/DT 비반복

    GpioCtrlRegs.GPAPUD.bit.GPIO30 = 0;  // Enable pullup on GPIO30
    GpioCtrlRegs.GPAMUX2.bit.GPIO30 = 0; // GPIO30 = GPIO30
    GpioCtrlRegs.GPADIR.bit.GPIO30 = 0;  // GPIO30 = input
    GpioDataRegs.GPASET.bit.GPIO30 = 0;   // ITSM

    GpioCtrlRegs.GPAPUD.bit.GPIO31 = 0;  // Enable pullup on GPIO31
    GpioCtrlRegs.GPAMUX2.bit.GPIO31 = 0; // GPIO31 = GPIO31
    GpioCtrlRegs.GPADIR.bit.GPIO31 = 0;  // GPIO31 = input
    GpioDataRegs.GPASET.bit.GPIO31 = 0;   // I2T

    GpioCtrlRegs.GPBPUD.bit.GPIO32 = 0;  // Enable pullup on GPIO32
    GpioCtrlRegs.GPBMUX1.bit.GPIO32 = 0; // GPIO32 = GPIO32
    GpioCtrlRegs.GPBDIR.bit.GPIO32 = 0;  // GPIO32 = input
    GpioDataRegs.GPBSET.bit.GPIO32 = 0;   // 전류시험

    GpioCtrlRegs.GPBPUD.bit.GPIO33 = 0;  // Enable pullup on GPIO33
    GpioCtrlRegs.GPBMUX1.bit.GPIO33 = 0; // GPIO33 = GPIO33
    GpioCtrlRegs.GPBDIR.bit.GPIO33 = 0;  // GPIO33 = input
    GpioDataRegs.GPBSET.bit.GPIO33 = 0;   // 전압시험

    EDIS;
}

Uint8 GetSwitchStatus()
{
	Uint8 SwitchStatus = 0;

	SwitchStatus += GpioDataRegs.GPBDAT.bit.GPIO33;   // 전압시험
	SwitchStatus <<= 1;
	SwitchStatus += GpioDataRegs.GPBDAT.bit.GPIO32;   // 전류시험
	SwitchStatus <<= 1;
	SwitchStatus += GpioDataRegs.GPADAT.bit.GPIO14;   // DI/DT 반복
	SwitchStatus <<= 1;
	SwitchStatus += GpioDataRegs.GPADAT.bit.GPIO15;   // DI/DT 비반복
	SwitchStatus <<= 1;
	SwitchStatus += GpioDataRegs.GPADAT.bit.GPIO30;   // ITSM
	SwitchStatus <<= 1;
	SwitchStatus += GpioDataRegs.GPADAT.bit.GPIO31;   // I2T

	return SwitchStatus;

}

Uint8 GetSetupSwitchStatus()
{
	int returnValue =1;
	if(!GpioDataRegs.GPADAT.bit.GPIO0)
	{
		returnValue=0;
	}

	return returnValue;
}
