/*
 * HAL_XINT.c
 *
 *  Created on: 2016. 3. 1.
 *      Author: gisu
 */


#include "HAL_XINT.h"

void InitXintGpio(void)
{
	EALLOW;
    // Make GPIO0 an input
//    GpioCtrlRegs.GPAPUD.bit.GPIO0 = 0;  // Enable pullup on GPIO0
//    GpioCtrlRegs.GPAMUX1.bit.GPIO0 = 0; // GPIO0 = GPIO0
//    GpioCtrlRegs.GPADIR.bit.GPIO0 = 0;  // GPIO0 = input
//    GpioDataRegs.GPASET.bit.GPIO0 = 0;   // Load output latch
//    GpioIntRegs.GPIOXINT1SEL.all = 0;

	GpioCtrlRegs.GPAMUX1.bit.GPIO12 = 0;		// 핀 기능선택: GPIO12  SW3002
	GpioCtrlRegs.GPAMUX2.bit.GPIO27 = 0;		// 핀 기능선택: GPIO27  SW3003
	GpioCtrlRegs.GPADIR.bit.GPIO12 = 0;			// GPIO12 입출력 선택: Input
	GpioCtrlRegs.GPADIR.bit.GPIO27 = 0;			// GPIO27 입출력 선택: Input



	GpioIntRegs.GPIOXINT1SEL.all = 12;
	GpioIntRegs.GPIOXINT2SEL.all = 27;

    EDIS;
}

void InitXint(void)
{
	XIntruptRegs.XINT1CR.bit.POLARITY = 0;      // XINT1 인터럽트 발생 조건 설정: 입력 신호의 하강 엣지
	XIntruptRegs.XINT2CR.bit.POLARITY = 1;      // XINT2 인터럽트 발생 조건 설정: 입력 신호의 상승 엣지

	XIntruptRegs.XINT1CR.bit.ENABLE = 1;        // XINT1 인터럽트 : Enable
	XIntruptRegs.XINT2CR.bit.ENABLE = 1;        // XINT2 인터럽트 : Enable

}
