/*
 * HAL_EncorderSW.c
 *
 *  Created on: 2016. 2. 26.
 *      Author: Gisu
 */
#include "HAL_ENcorderSW.h"

void InitEncorderGIPO(){

	/* for Qep */
	EALLOW;

	GpioCtrlRegs.GPAPUD.bit.GPIO24 = 0;		/* Enable pull-up on GPIO24 (EQEP2A) */
	GpioCtrlRegs.GPAPUD.bit.GPIO25 = 0;		/* Enable pull-up on GPIO25 (EQEP2B) */
	GpioCtrlRegs.GPAPUD.bit.GPIO26 = 0;		/* Enable pull-up on GPIO26 (EQEP2I) */

	GpioCtrlRegs.GPACTRL.bit.QUALPRD3 = 0xFF;	/* Specifies the qualification sampling period for GPIO24 to GPIO31 */
	GpioCtrlRegs.GPAQSEL2.bit.GPIO24 = 2;		/* Qualification using 6 samples (EQEP2A) */
	GpioCtrlRegs.GPAQSEL2.bit.GPIO25 = 2;		/* Qualification using 6 samples (EQEP2B) */
	GpioCtrlRegs.GPAQSEL2.bit.GPIO26 = 2;		/* Qualification using 6 samples (EQEP2I) */


	GpioCtrlRegs.GPAMUX2.bit.GPIO24 = 2;	/* Configure GPIO24 as EQEP2A */
	GpioCtrlRegs.GPAMUX2.bit.GPIO25 = 2;	/* Configure GPIO25 as EQEP2B */
	GpioCtrlRegs.GPAMUX2.bit.GPIO26 = 2;	/* Configure GPIO26 as EQEP2I */


	GpioCtrlRegs.GPADIR.bit.GPIO24 = 0;		/* DIRECTION Input GPIO24 (EQEP2I) */
	GpioCtrlRegs.GPADIR.bit.GPIO25 = 0;		/* DIRECTION Input GPIO25 (EQEP2I) */
	GpioCtrlRegs.GPADIR.bit.GPIO26 = 0;		/* DIRECTION Input GPIO26 (EQEP2I) */
//
//	GpioCtrlRegs.GPAMUX2.bit.GPIO24 = 0;	/* Configure GPIO24 as GPIO */
//	GpioCtrlRegs.GPAMUX2.bit.GPIO25 = 0;	/* Configure GPIO25 as GPIO */
//	GpioCtrlRegs.GPAMUX2.bit.GPIO26 = 0;	/* Configure GPIO26 as GPIO */

	EDIS;

}

void InitEncorderEQep2(Uint32 PositionMax)
{
	EQep2Regs.QDECCTL.bit.QSRC = 0;			/* Quadrature count mode */
	EQep2Regs.QEPCTL.bit.FREE_SOFT = 2;
	EQep2Regs.QEPCTL.bit.PCRM = 1;			/* Position Counter Reset on Maximum Position */
	EQep2Regs.QPOSMAX = PositionMax - 1;	/* 24 x 4 QCLK @ 1 revolution */
	EQep2Regs.QEPCTL.bit.QPEN = 1; 			/* QEP enable */
}



//		if(GpioDataRegs.GPADAT.bit.GPIO24==1){
//			if(GpioDataRegs.GPADAT.bit.GPIO25==1){
//				ClearLCD();
//				SetStringFull("UP");
//				DELAY_US(100000);
//			}
//			else{
//				ClearLCD();
//				SetStringFull("DOWN");
//				DELAY_US(100000);
//			}
//		}
//		else{
//
//		}
