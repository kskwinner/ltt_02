/*
 * HAL_XINT.h
 *
 *  Created on: 2016. 3. 1.
 *      Author: gisu
 */

#include "PF_Configure.h"

#ifndef HAL_INCLUDE_HAL_XINT_H_
#define HAL_INCLUDE_HAL_XINT_H_

void InitXintGpio(void);
void InitXint(void);


#endif /* HAL_INCLUDE_HAL_XINT_H_ */
