/*
 * HAL_ADC.h
 *
 *  Created on: 2016. 7. 15.
 *      Author: gisu
 */
#include "DSP2833x_Device.h"     	// DSP2833x Headerfile Include File
#include "DSP2833x_Examples.h"   	// DSP2833x Examples Include File

#ifndef HAL_ADC_H_
#define HAL_ADC_H_

void ConfigADC();
void ADC_GPIO_INIT();



#endif /* HAL_ADC_H_ */
