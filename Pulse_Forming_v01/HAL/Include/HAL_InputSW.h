/*
 * HAL_InputSW.h
 *
 *  Created on: 2016. 9. 9.
 *      Author: gisu
 */

#ifndef HAL_INPUTSW_H_
#define HAL_INPUTSW_H_

#include "DSP2833x_Device.h"     // DSP2833x Headerfile Include File
#include "DSP2833x_Examples.h"   // DSP2833x Examples Include File

void InitSwitchGPIO();
Uint8 GetSwitchStatus();
Uint8 GetSetupSwitchStatus();


#endif /* HAL_INPUTSW_H_ */
