/**
 * File Name : 
 * File Description :
 * Copyright Sentence
 * Author : GSKIM
 * Dept :
 * Created Date : 2016.02.123
 * Version : v0.9
 */

#include <stdlib.h>
#include "PF_Configure.h"

#ifndef HAL_RS422_H_
#define HAL_RS422_H_

//----- STRING CMD---------------
#define CMD_INIT			0xe4
#define	CMD_POS_SET			0xe1
#define	CMD_FULL_STX		0xe2
#define	CMD_FULL_ETX		0xd5
#define	CMD_FULL_ADD		0xe6
#define	CMD_LCD_CLEAR		0xe5
#define	CMD_HARF_STX		0xe7
#define	CMD_HARF_ADD		0xe8
#define	CMD_DISPLAY			0xe0
#define	CMD_LCD_L1_CLEAR	0xd8
#define	CMD_LCD_L2_CLEAR	0xd9

//----- CURSOR CMD---------------
#define CMD_C_HOME			0xeb
#define CMD_C_ON			0xec
#define CMD_C_BLINK			0xed
#define CMD_C_REVERSE		0xef

//----- MODE CMD---------------
#define CMD_M_NORMAL		0xf1
#define CMD_M_REVERSE		0xf2
#define CMD_M_BLINK			0xf3
#define CMD_M_RE_BK			0xf4
#define CMD_M_BK_ON			0xf5
#define CMD_M_BK_OFF		0xf6

//----- SCROLL CMD---------------
#define CMD_SCR_UP			0xf7
#define CMD_SCR_DOWN		0xf8
#define CMD_SCR_RIGHT		0xf9
#define CMD_SCR_LEFT		0xfa


void InitXD1602SerialGpio(void);
void InitXD1602Comm(void);
void SendByte(char a);
void SendString(char * msg);
void DemoMode();
void InItLCD();
void ClearLCD();
void ClearLCDL1();
void ClearLCDL2();
void CursorOn();
void CursorBlink();
void CursorReverse();
void TestMSG();
void SetPosition(int x, int y);
void SetStringFull(char* str);
void SetStringFullAdd(char* str);
void SetStringHarf(char* str);
void SetStringHarfAdd(char* str);

void SendInt(long value);


unsigned char Hex2AsciiHigh( unsigned char a);
unsigned char Hex2AsciiLow( unsigned char a);
unsigned char Ascii2Hex( unsigned char a, unsigned char b );

#endif /* HAL_RS422_H_ */



//===========================================================================
// End of file.
//===========================================================================




