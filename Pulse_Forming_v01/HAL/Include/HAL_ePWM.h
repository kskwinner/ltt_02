/*
 * HAL_ePWM.h
 *
 *  Created on: 2016. 3. 1.
 *      Author: gisu
 */

#include "PF_Configure.h"

#ifndef HAL_INCLUDE_HAL_EPWM_H_
#define HAL_INCLUDE_HAL_EPWM_H_


void InitEPWM1GPIO(void);
void InitEPwm1Module(void);


#endif /* HAL_INCLUDE_HAL_EPWM_H_ */
