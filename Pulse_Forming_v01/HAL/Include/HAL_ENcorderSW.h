/*
 * HAL_ENcorderSW.h
 *
 *  Created on: 2016. 2. 26.
 *      Author: Gisu
 */
#include "DSP2833x_Device.h"     	// DSP2833x Headerfile Include File
#include "DSP2833x_Examples.h"   	// DSP2833x Examples Include File

#ifndef HAL_ENCORDERSW_H_
#define HAL_ENCORDERSW_H_

#define	ENCODER_REV		24			/* Pulse/Revolution */



void InitEncorderGIPO();
void InitEncorderEQep2(Uint32 PositionMax);


#endif /* HAL_ENCORDERSW_H_ */
