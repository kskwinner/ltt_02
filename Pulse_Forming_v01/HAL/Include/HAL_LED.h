//******************************************************************************
//!
//!
//!
//******************************************************************************
/***************************************************************************//**
 * @file       HAL_LED.h
 * @addtogroup HAL_LED
 * @{
 ******************************************************************************/
#include "PF_Configure.h"

#ifndef SCU_HAL_LED_H_
#define SCU_HAL_LED_H_

void InitLEDGpio();



#define		DSP_HBEAT_TOGGLE() 	 	 	 	 	(GpioDataRegs.GPATOGGLE.bit.GPIO2=1)


#define 	PHASE_DETECT_HIGH()				(GpioDataRegs.GPASET.bit.GPIO4 = 1)
#define     PHASE_DETECT_LOW()				(GpioDataRegs.GPACLEAR.bit.GPIO4 = 1)
#define		PHASE_DETECT_TOGGLE()	 	 	(GpioDataRegs.GPATOGGLE.bit.GPIO4=1)


#endif /* SCU_HAL_LED_H_ */

