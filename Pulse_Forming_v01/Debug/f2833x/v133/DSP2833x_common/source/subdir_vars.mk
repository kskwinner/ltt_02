################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../f2833x/v133/DSP2833x_common/source/DSP2833x_ADC_cal.asm \
../f2833x/v133/DSP2833x_common/source/DSP2833x_CSMPasswords.asm \
../f2833x/v133/DSP2833x_common/source/DSP2833x_CodeStartBranch.asm \
../f2833x/v133/DSP2833x_common/source/DSP2833x_DBGIER.asm \
../f2833x/v133/DSP2833x_common/source/DSP2833x_DisInt.asm \
../f2833x/v133/DSP2833x_common/source/DSP2833x_usDelay.asm 

C_SRCS += \
../f2833x/v133/DSP2833x_common/source/DSP2833x_Adc.c \
../f2833x/v133/DSP2833x_common/source/DSP2833x_CpuTimers.c \
../f2833x/v133/DSP2833x_common/source/DSP2833x_DMA.c \
../f2833x/v133/DSP2833x_common/source/DSP2833x_DefaultIsr.c \
../f2833x/v133/DSP2833x_common/source/DSP2833x_ECan.c \
../f2833x/v133/DSP2833x_common/source/DSP2833x_ECap.c \
../f2833x/v133/DSP2833x_common/source/DSP2833x_EPwm.c \
../f2833x/v133/DSP2833x_common/source/DSP2833x_EQep.c \
../f2833x/v133/DSP2833x_common/source/DSP2833x_Gpio.c \
../f2833x/v133/DSP2833x_common/source/DSP2833x_I2C.c \
../f2833x/v133/DSP2833x_common/source/DSP2833x_Mcbsp.c \
../f2833x/v133/DSP2833x_common/source/DSP2833x_MemCopy.c \
../f2833x/v133/DSP2833x_common/source/DSP2833x_PieCtrl.c \
../f2833x/v133/DSP2833x_common/source/DSP2833x_PieVect.c \
../f2833x/v133/DSP2833x_common/source/DSP2833x_Sci.c \
../f2833x/v133/DSP2833x_common/source/DSP2833x_Spi.c \
../f2833x/v133/DSP2833x_common/source/DSP2833x_SysCtrl.c \
../f2833x/v133/DSP2833x_common/source/DSP2833x_Xintf.c 

C_DEPS += \
./f2833x/v133/DSP2833x_common/source/DSP2833x_Adc.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_CpuTimers.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_DMA.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_DefaultIsr.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_ECan.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_ECap.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_EPwm.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_EQep.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_Gpio.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_I2C.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_Mcbsp.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_MemCopy.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_PieCtrl.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_PieVect.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_Sci.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_Spi.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_SysCtrl.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_Xintf.d 

OBJS += \
./f2833x/v133/DSP2833x_common/source/DSP2833x_ADC_cal.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_Adc.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_CSMPasswords.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_CodeStartBranch.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_CpuTimers.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_DBGIER.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_DMA.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_DefaultIsr.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_DisInt.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_ECan.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_ECap.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_EPwm.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_EQep.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_Gpio.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_I2C.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_Mcbsp.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_MemCopy.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_PieCtrl.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_PieVect.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_Sci.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_Spi.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_SysCtrl.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_Xintf.obj \
./f2833x/v133/DSP2833x_common/source/DSP2833x_usDelay.obj 

ASM_DEPS += \
./f2833x/v133/DSP2833x_common/source/DSP2833x_ADC_cal.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_CSMPasswords.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_CodeStartBranch.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_DBGIER.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_DisInt.d \
./f2833x/v133/DSP2833x_common/source/DSP2833x_usDelay.d 

OBJS__QUOTED += \
"f2833x\v133\DSP2833x_common\source\DSP2833x_ADC_cal.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_Adc.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_CSMPasswords.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_CodeStartBranch.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_CpuTimers.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_DBGIER.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_DMA.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_DefaultIsr.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_DisInt.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_ECan.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_ECap.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_EPwm.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_EQep.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_Gpio.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_I2C.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_Mcbsp.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_MemCopy.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_PieCtrl.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_PieVect.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_Sci.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_Spi.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_SysCtrl.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_Xintf.obj" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_usDelay.obj" 

C_DEPS__QUOTED += \
"f2833x\v133\DSP2833x_common\source\DSP2833x_Adc.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_CpuTimers.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_DMA.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_DefaultIsr.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_ECan.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_ECap.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_EPwm.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_EQep.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_Gpio.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_I2C.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_Mcbsp.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_MemCopy.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_PieCtrl.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_PieVect.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_Sci.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_Spi.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_SysCtrl.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_Xintf.d" 

ASM_DEPS__QUOTED += \
"f2833x\v133\DSP2833x_common\source\DSP2833x_ADC_cal.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_CSMPasswords.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_CodeStartBranch.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_DBGIER.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_DisInt.d" \
"f2833x\v133\DSP2833x_common\source\DSP2833x_usDelay.d" 

ASM_SRCS__QUOTED += \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_ADC_cal.asm" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_CSMPasswords.asm" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_CodeStartBranch.asm" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_DBGIER.asm" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_DisInt.asm" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_usDelay.asm" 

C_SRCS__QUOTED += \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_Adc.c" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_CpuTimers.c" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_DMA.c" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_DefaultIsr.c" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_ECan.c" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_ECap.c" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_EPwm.c" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_EQep.c" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_Gpio.c" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_I2C.c" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_Mcbsp.c" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_MemCopy.c" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_PieCtrl.c" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_PieVect.c" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_Sci.c" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_Spi.c" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_SysCtrl.c" \
"../f2833x/v133/DSP2833x_common/source/DSP2833x_Xintf.c" 


