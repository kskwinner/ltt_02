/*
 * main.c
 */

#include "HAL_InputSW.h"
#include "HAL_ADC.h"
#include "HAL_LED.h"
#include "HAL_XD1602.h"
#include "HAL_ENcorderSW.h"
#include "HAL_XINT.h"
#include "HAL_ePWM.h"
#include "PF_Configure.h"
#include "DSP28x_Project.h"

#define BUFFSIZE 100

//interrupt void ISR_PhaseDetect(void);
interrupt void ISR_CpuTimer0(void);

interrupt void Xint1_isr(void);
interrupt void Xint2_isr(void);
interrupt void epwm1_isr(void);
interrupt void adc_isr(void);					// ADC 인터럽트 함수 선언

//void IdelModeRunning();
void ModeSwitch(Uint8 ModeValue);
void LCDUpdate();

//****************************************************
//Major global variable

SelectTest g_CurrentMode;




//****************************************************

Uint16 g_pwmPulseCNT;
Uint16 g_CNT_eq_PRD;

Uint16 g_SwitchStatus;
Uint16 g_KeyOkPushed;
Uint16 g_SelectConfig;
Uint8 PulseOputFlag;
Uint8 Xint1PushCNT;
Uint8 Xint2PushCNT;
Uint8 IntoReadyFlag =0;
#define	ENCODER_REV		24			/* Pulse/Revolution */

#if DEBUG
Uint8 Modevalue =63;
#endif

Uint32	PositionMax;
Uint32	PositionCounter;
Uint32	RotateDirection;
float32	RotateAngleUnit;
float32	RotateAngleDegree;

Uint16 g_pre_value;
Uint16 g_time;
Uint16 g_wait;
Uint16 g_vector[BUFFSIZE];
Uint16 g_count;
Uint16 g_okcount;
Uint16 g_break;
Uint16 g_maincount;
Uint16 g_subcount;
Uint16 g_flag;
Uint16 g_push;
Uint16 g_pushed;
Uint16 ADC_value;

//Deadband 적용
//****************************************************


//#define	SYSTEM_CLOCK		150E6	/* 150MHz */
//#define	TBCLK				150E6	/* 150MHz */
//#define	PWM_CARRIER			100E3	/* 100kHz */
//#define	PWM_DUTY_RATIO_A	5E-1	/* 0.5, 50% */


//Uint16	BackTicker;
//Uint16	EPwm1IsrTicker;
//
//float32	PwmCarrierFrequency;
//float32	PwmDutyRatioA;
//float32	FallingEdgeDelay;
//float32	RisingEdgeDelay;

//****************************************************

DeviceStatus RunState;		//State
SelectTest Mode;			//Mode


//****************************************************

Uint8 SwitchError;
Uint8 isSwitchChange;
Uint8 isSetupSwitchChange;
Uint8 isChageEncorder;
long PulseWidth=0;
int pulsetest = 0;
Uint8 g_WheelFlag=0;
Uint8 trigger=0;
Uint8 preprocess=0;
Uint8 repeat_Triger=0;
Uint8 repeat_Counter=0;

int main(void) {
	
	// Step 1. Initialize System Control:
	// PLL, WatchDog, enable Peripheral Clocks
	// This example function is found in the DSP2833x_SysCtrl.c file.
	InitSysCtrl();

	EALLOW;
#if (CPU_FRQ_150MHZ)     // Default - 150 MHz SYSCLKOUT
#define ADC_MODCLK 0x3 // HSPCLK = SYSCLKOUT/2*ADC_MODCLK2 = 150/(2*3)   = 25.0 MHz
#endif
#if (CPU_FRQ_100MHZ)
#define ADC_MODCLK 0x2 // HSPCLK = SYSCLKOUT/2*ADC_MODCLK2 = 100/(2*2)   = 25.0 MHz
#endif
	EDIS;

	// Define ADCCLK clock frequency ( less than or equal to 25 MHz )
	// Assuming InitSysCtrl() has set SYSCLKOUT to 150 MHz
	EALLOW;
	SysCtrlRegs.HISPCP.all = ADC_MODCLK;
	EDIS;


	// Step 2. Initalize GPIO:
	InitSwitchGPIO();
	InitEncorderGIPO();
	InitXD1602SerialGpio();
	InitLEDGpio();
	InitXintGpio();
//	InitEPWM1GPIO();
//	ADC_GPIO_INIT();

	EALLOW;
    GpioCtrlRegs.GPAPUD.bit.GPIO1 = 0;  // Enable pullup on GPIO1
    GpioCtrlRegs.GPAMUX1.bit.GPIO1 = 0; // GPIO1 = GPIO2
    GpioCtrlRegs.GPADIR.bit.GPIO1 = 1;  // GPIO1 = output
    GpioDataRegs.GPASET.bit.GPIO1 = 0;   // 전류

    GpioCtrlRegs.GPAPUD.bit.GPIO2 = 0;  // Enable pullup on GPIO2
    GpioCtrlRegs.GPAMUX1.bit.GPIO2 = 0; // GPIO2 = GPIO2
    GpioCtrlRegs.GPADIR.bit.GPIO2 = 1;  // GPIO2 = output
    GpioDataRegs.GPASET.bit.GPIO2 = 0;   // didt/반복

    GpioCtrlRegs.GPAPUD.bit.GPIO3 = 0;  // Enable pullup on GPIO3
    GpioCtrlRegs.GPAMUX1.bit.GPIO3 = 0; // GPIO3 = GPIO2
    GpioCtrlRegs.GPADIR.bit.GPIO3 = 1;  // GPIO3 = output
    GpioDataRegs.GPASET.bit.GPIO3 = 0;   // didi/비반복 , i2t, itsm

    EDIS;
	// Step 3. Clear all interrupts and initialize PIE vector table:
	// Disable CPU interrupts
	DINT;




	// Initialize the PIE control registers to their default state.
	// The default state is all PIE interrupts disabled and flags
	// are cleared.
	// This function is found in the DSP2833x_PieCtrl.c file.
	InitPieCtrl();



	// Disable CPU interrupts and clear all CPU interrupt flags:
	IER = 0x0000;
	IFR = 0x0000;

	// Initialize the PIE vector table with pointers to the shell Interrupt
	// Service Routines (ISR).
	// This will populate the entire table, even if the interrupt
	// is not used in this example.  This is useful for debug purposes.
	// The shell ISR routines are found in DSP2833x_DefaultIsr.c.
	// This function is found in DSP2833x_PieVect.c.
	InitPieVectTable();

	// Interrupts that are used in this example are re-mapped to
	// ISR functions found within this file.
	EALLOW;  // This is needed to write to EALLOW protected registers
	PieVectTable.TINT0      = &ISR_CpuTimer0;
	PieVectTable.XINT1 		= &Xint1_isr;
	PieVectTable.XINT2 		= &Xint2_isr;
//	PieVectTable.EPWM1_INT 	= &epwm1_isr;
//	PieVectTable.ADCINT 	= &adc_isr;
	EDIS;    // This is needed to disable write to EALLOW protected registers


	//----Flash 다운로드시 반드시 활성화 할껏
	#if( RUN_ON_TYPE==RUN_ON_FLASH)
		// Copy time critical code and Flash setup code to RAM
		// This includes the following ISR functions: epwm1_timer_isr(), epwm2_timer_isr()
		// epwm3_timer_isr and and InitFlash();
		// The  RamfuncsLoadStart, RamfuncsLoadEnd, and RamfuncsRunStart
		// symbols are created by the linker. Refer to the F28335.cmd file.
		MemCopy(&RamfuncsLoadStart, &RamfuncsLoadEnd, &RamfuncsRunStart);

		// Call Flash Initialization to setup flash waitstates
		// This function must reside in RAM
		InitFlash();

	#endif

	// Step 4. Initialize the Device Peripheral. This function can be
	InitXint();
	InitXD1602Comm();
	InitCpuTimers();
//	InitEPwm1Module();
	InitAdc();
//	ConfigADC();



	// Configure CPU-Timer 0
	// 150MHz CPU Freq,  (in uSeconds)
	ConfigCpuTimer(&CpuTimer0, 150, 10);
//	ConfigCpuTimer(&CpuTimer1, 150, 1); 				                    // 50Hz for  1553b  communication
//	ConfigCpuTimer(&CpuTimer2, 150, 1000/(ISR_FREQUENCY*2)); 					// 2khz
	// To ensure precise timing, use write-only instructions to write to the entire register. Therefore, if any
	// of the configuration bits are changed in ConfigCpuTimer and InitCpuTimers (in DSP2833x_CpuTimers.h), the
	// below settings must also be updated.

	StartCpuTimer0();//for ADC, RDC		    1khz
//	StartCpuTimer1();//for 1553B		    50Hz
//	StartCpuTimer2();//for CSU, CAN		    2kHz



	// Step 5. Initialize HAL level



	// Enable TINT0 in the PIE Group 1, INT7
    PieCtrlRegs.PIEIER1.bit.INTx7 = 1;


	// PIE의 ADC 인터럽트 활성화
//	PieCtrlRegs.PIEIER1.bit.INTx6 = 1;			// PIE 인터럽트(ADCINT) 활성화

    // Enable xint1  in the PIE Group 1( INT4, INT5) PIE Group 12 (INT1)
	PieCtrlRegs.PIEIER1.bit.INTx4 = 1;			// PIE 인터럽트(XINT1) : Enable
	PieCtrlRegs.PIEIER1.bit.INTx5 = 1;			// PIE 인터럽트(XINT2) : Enable
//	PieCtrlRegs.PIEIER3.bit.INTx1 = 1;			// PIE 인터럽트(EPWM1_INT) : Enable

	IER |= M_INT1 | M_INT3;						// CPU 인터럽트(INT1)  활성화

	// 외부 인터터트 포합된 백터 활성화






    // Enable global Interrupts and higher priority real-time debug events:
	EINT;   	        // Enable Global interrupt INTM
	ERTM;  	            // Enable Global realtime interrupt DBGM


	// ADC 설정
	AdcRegs.ADCTRL3.bit.ADCCLKPS = 3;	   		// ADCCLK = HSPCLK/(ADCCLKPS*2)/(CPS+1)
	AdcRegs.ADCTRL1.bit.CPS = 1;				// ADCCLK = 75MHz/(3*2)/(1+1) = 6.25MHz
	AdcRegs.ADCTRL1.bit.ACQ_PS = 3;				// 샘플/홀드 사이클 = ACQ_PS + 1 = 4 (ADCCLK기준)
	AdcRegs.ADCTRL1.bit.SEQ_CASC = 1;			// 시퀀스 모드 설정: 직렬 시퀀스 모드 (0:병렬 모드, 1:직렬 모드)
	AdcRegs.ADCMAXCONV.bit.MAX_CONV1 = 0;   	// ADC 채널수 설정: 1개(=MAX_CONV+1)채널을 ADC
	AdcRegs.ADCCHSELSEQ1.bit.CONV00 = 0; 		// ADC 순서 설정: 첫번째로 ADCINA0 채널을 ADC

	int i;
	DELAY_US(1000000);
	InItLCD();
	SetStringHarf("Pulse Formming Machine");
	SetPosition(0,1);
	DELAY_US(500000);
	for(i=0;i<16;i++)
	{
		SetPosition(i,1);
		SetStringHarf("*");
		DELAY_US(50000);
	}
	RunState = IdleMode;
	g_pwmPulseCNT = 60;


	PositionMax = ENCODER_REV * 4;
	PositionCounter = 0;
	RotateDirection = 0;
	RotateAngleUnit = 0;
	RotateAngleDegree = 0;
	InitEncorderEQep2(PositionMax);


//	int isChageEncorder=0;
//	int lastCounter=0;
//	Uint16 KeyLock=0;
	g_KeyOkPushed =0;
//	Uint16 g_SelectConfig=0;
//	Uint16 SelectMode=0;

	g_pushed=0;
	g_push=0;
	g_subcount=0;


	pulsetest=10;
	PulseOputFlag=0;

	Xint1PushCNT=0;
	Xint2PushCNT=0;
	IntoReadyFlag=0;

	preprocess =1;
	repeat_Triger=0;
	repeat_Counter=0;

	while(1){




		if(RunState==ReadyMode && Mode==didtRepeatMode)
		{
			AdcRegs.ADCTRL2.bit.SOC_SEQ1 = 1;					// ADC 시퀀스 시작
			DELAY_US(1.72L);									// ADC 시퀀스 변환시간(약1.72usec)만큼 지연

			ADC_value = AdcRegs.ADCRESULT0;						// ADC 결과 저장

			if(repeat_Triger==1)
			{

				if(g_pre_value < ADC_value)
				{
					if(ADC_value < 10000) g_wait=0;
					if(37000 < ADC_value && g_wait ==0)
					{
						if(repeat_Counter<60)
						{
							repeat_Counter++;
							DELAY_US(2000);
							GpioDataRegs.GPATOGGLE.bit.GPIO2 = 1;
							DELAY_US(10);
							GpioDataRegs.GPATOGGLE.bit.GPIO2 = 1;
							g_maincount++;
							g_wait=1;
							g_flag=1;
						}
						else
						{
							repeat_Triger=0;
							repeat_Counter=0;
							RunState=Triggered;
							LCDUpdate();
						}
					}
				}

			}

			g_pre_value = ADC_value;
			g_vector[g_count]= ADC_value;
			g_count++;

			if(g_count>2048) g_count=0;

			AdcRegs.ADCTRL2.bit.RST_SEQ1 = 1;					// ADC 시퀀서 리셋
			DELAY_US(0.64L);									// ADC 시퀀서 리셋시간(약0.64usec)만큼 지연
		}

		if(IntoReadyFlag==1)
		{

			LCDUpdate();
			DELAY_US(1000000);
			IntoReadyFlag=0;
		}

		if(isSwitchChange)
		{
			//1.LCD 변경
			LCDUpdate();
			isSwitchChange=0;



		}

		if(isSetupSwitchChange)
		{
			LCDUpdate();
			isSetupSwitchChange=0;
		}


		if(isChageEncorder>0 && g_WheelFlag==1)
		{

			switch(RunState){
			case IdleMode:
				break;
			case RunningMode:
				break;
			case ConfigMode:
				switch (Mode){
				case CurrentMode:
					if(isChageEncorder==1)
					{
						PulseWidth += 100000;


						if(PulseWidth >= 2000000)
						{
							PulseWidth =2000000;
						}

						LCDUpdate();
					}
					else
					{
						PulseWidth -= 100000;

						if(PulseWidth <= 500000)
						{
							PulseWidth =500000;
						}

						LCDUpdate();
					}
					break;
				case didtNonRepeatMode:
					if(isChageEncorder==1)
					{
						PulseWidth += 10;


						if(PulseWidth >= 100)
						{
							PulseWidth =100;
						}

						LCDUpdate();
					}
					else
					{
						PulseWidth -= 10;

						if(PulseWidth <= 10)
						{
							PulseWidth =10;
						}

						LCDUpdate();
					}
					break;
				case ITSMMode:
					if(isChageEncorder==1)
					{
						PulseWidth += 10;


						if(PulseWidth >= 100)
						{
							PulseWidth =100;
						}

						LCDUpdate();
					}
					else
					{
						PulseWidth -= 10;

						if(PulseWidth <= 10)
						{
							PulseWidth =10;
						}

						LCDUpdate();
					}
					break;
				case I2TMode:
					if(isChageEncorder==1)
					{
						PulseWidth += 10;


						if(PulseWidth >= 100)
						{
							PulseWidth =100;
						}

						LCDUpdate();
					}
					else
					{
						PulseWidth -= 10;

						if(PulseWidth <= 10)
						{
							PulseWidth =10;
						}

						LCDUpdate();
					}
					break;
				default:
					break;
				}
				break;

			default:
				break;
			}

			isChageEncorder=0;
		}


	}
}


void LCDUpdate()
{
	switch(RunState){
	case IdleMode:
		ClearLCD();
		SetPosition(0,0);
		if(Mode==NotSelect)
		{
			SetStringFull("시험 스위치를 선택 하세요");
		}
		else
		{
			SetPosition(0,0);
			SetStringFull("ERROR");
			SetPosition(0,1);
			SetStringFull("한개의 스위치만 ON 하세요");
		}
		break;
	case RunningMode:
		ClearLCD();
		SetPosition(1,0);
		g_WheelFlag=0;
		switch (Mode){

		case VoltageMode:
			SetStringFull("전압 모드");
			break;
		case CurrentMode:
			SetStringFull("전류 모드");
			SetPosition(0,1);
			SetStringHarf("Pulse Width : ");
			SendInt(PulseWidth/1000);
			SetStringHarf(" ms               ");
			break;
		case didtRepeatMode:
			SetStringFull("di/dt 반복모드");
			SetPosition(0,1);
			SetStringHarf("Pulse Width : ");
			SendInt(PulseWidth);
			SetStringHarf(" us               ");
			break;
		case didtNonRepeatMode:
			SetStringFull("di/dt 비반복모드");
			SetPosition(0,1);
			SetStringHarf("Pulse Width : ");
			SendInt(PulseWidth);
			SetStringHarf(" us                ");
			break;
		case ITSMMode:
			SetStringFull("ITSM 모드");
			SetPosition(0,1);
			SetStringHarf("Pulse Width : ");
			SendInt(PulseWidth);
			SetStringHarf(" us                ");
			break;
		case I2TMode:
			SetStringFull("I2T 모드");
			SetPosition(0,1);
			SetStringHarf("Pulse Width : ");
			SendInt(PulseWidth);
			SetStringHarf(" us ");
			break;
		default:

			break;
		}
		break;
	case ConfigMode:
		if(Mode==CurrentMode)
		{
			g_WheelFlag=1;
			ClearLCDL1();
			SetPosition(0,0);
			CursorBlink();
			SetStringFull("값을 변경 하세요!");
			ClearLCDL2();
			SetPosition(0,1);

			SetStringHarf("Pulse Width : ");

			SendInt(PulseWidth/1000);
			SetStringHarf(" ms           ");

		}
		else if(Mode==didtNonRepeatMode)
		{
			g_WheelFlag=1;
			ClearLCDL1();
			SetPosition(0,0);
			CursorBlink();
			SetStringFull("값을 변경 하세요!");
			ClearLCDL2();
			SetPosition(0,1);
			SetStringHarf("Pulse Width : ");
			SendInt(PulseWidth);
			SetStringHarf(" us                ");
		}
		else if(Mode==ITSMMode)
		{
			g_WheelFlag=1;
			ClearLCDL1();
			SetPosition(0,0);
			CursorBlink();
			SetStringFull("값을 변경 하세요!");
			ClearLCDL2();
			SetPosition(0,1);
			SetStringHarf("Pulse Width : ");
			SendInt(PulseWidth);
			SetStringHarf(" us               ");
		}
		else if(Mode==I2TMode)
		{
			g_WheelFlag=1;
			ClearLCDL1();
			SetPosition(0,0);
			CursorBlink();
			SetStringFull("값을 변경 하세요!");

			ClearLCDL2();
			SetPosition(0,1);
			SetStringHarf("Pulse Width : ");
			SendInt(PulseWidth);
			SetStringHarf(" us                ");
		}
//		SetPosition(1,1);
//		SetStringFull("설정");
//		CursorBlink();
//		SetPosition(5,1);
//		SetStringFull("설정");

		break;
	case ReadyMode:
		if(Mode==CurrentMode)
		{
			SetPosition(0,0);
			CursorBlink();
			SetStringFull("준비완료! 트리거 하세요.");
			ClearLCDL2();
			SetPosition(0,1);

			SetStringHarf("Pulse Width : ");

			SendInt(PulseWidth/1000);
			SetStringHarf(" ms   ");

		}
		else if(Mode==didtNonRepeatMode)
		{

			ClearLCDL1();
			SetPosition(0,0);
			CursorBlink();
			SetStringFull("준비완료! 트리거 하세요.");
			ClearLCDL2();
			SetPosition(0,1);
			SetStringHarf("Pulse Width : ");
			SendInt(PulseWidth);
			SetStringHarf(" us            ");
		}
		else if(Mode==didtRepeatMode)
		{

			ClearLCDL1();
			SetPosition(0,0);
			CursorBlink();
			SetStringFull("준비완료! 트리거 하세요.");
			ClearLCDL2();
			SetPosition(0,1);
			SetStringHarf("Pulse Width : ");
			SendInt(PulseWidth);
			SetStringHarf(" us   ");
		}
		else if(Mode==ITSMMode)
		{

			ClearLCDL1();
			SetPosition(0,0);
			CursorBlink();
			SetStringFull("준비완료! 트리거 하세요.");
			ClearLCDL2();
			SetPosition(0,1);
			SetStringHarf("Pulse Width : ");
			SendInt(PulseWidth);
			SetStringHarf(" us               ");
		}
		else if(Mode==I2TMode)
		{
			g_WheelFlag=1;
			ClearLCDL1();
			SetPosition(0,0);
			CursorBlink();
			SetStringFull("준비완료! 트리거 하세요.");
			ClearLCDL2();
			SetPosition(0,1);
			SetStringHarf("Pulse Width : ");
			SendInt(PulseWidth);
			SetStringHarf(" us                ");
		}
		break;
	case Triggered:
		if(Mode==CurrentMode)
		{
			ClearLCDL1();
			SetPosition(0,0);
			CursorBlink();
			SetStringFull("완료! ");
			ClearLCDL2();
			SetPosition(0,1);

			SetStringHarf("Pulse Width : ");

			SendInt(PulseWidth/1000);
			SetStringHarf(" ms             ");

		}
		else if(Mode==didtNonRepeatMode)
		{
			ClearLCDL1();
			SetPosition(0,0);
			CursorBlink();
			SetStringFull("완료!");
			ClearLCDL2();
			SetPosition(0,1);
			SetStringHarf("Pulse Width : ");
			SendInt(PulseWidth);
			SetStringHarf(" us                ");
		}
		else if(Mode==ITSMMode)
		{
			ClearLCDL1();
			SetPosition(0,0);
			CursorBlink();
			SetStringFull("완료!");
			ClearLCDL2();
			SetPosition(0,1);
			SetStringHarf("Pulse Width : ");
			SendInt(PulseWidth);
			SetStringHarf(" us                  ");
		}
		else if(Mode==I2TMode)
		{
			ClearLCDL1();
			SetPosition(0,0);
			CursorBlink();
			SetStringFull("완료!");
			ClearLCDL2();
			SetPosition(0,1);
			SetStringHarf("Pulse Width : ");
			SendInt(PulseWidth);
			SetStringHarf(" us               ");
		}
		else if(Mode==didtRepeatMode)
		{
			ClearLCDL1();
			SetPosition(0,0);
			CursorBlink();
			SetStringFull("완료!");
			ClearLCDL2();
			SetPosition(0,1);
			SetStringHarf("Pulse Width : ");
			SendInt(PulseWidth);
			SetStringHarf(" us                ");
		}
		break;
	}

}

interrupt void Xint1_isr(void)
{
	XIntruptRegs.XINT1CR.bit.ENABLE = 0;

	if(IntoReadyFlag==0)
	{

		IntoReadyFlag = 1;

		if(RunState==RunningMode)
		{
			RunState = ReadyMode;
			if(Mode==didtRepeatMode)
			{
			    EALLOW;
				SysCtrlRegs.PCLKCR0.bit.ADCENCLK = 1;
				EDIS;
			}

		}
		else if(RunState==ReadyMode)
		{
			RunState = RunningMode;

		}
		else if(RunState==Triggered)
		{
			RunState = ReadyMode;

		}

	}

//	RunState++;
//	if(RunState>ConfigMode)
//	{
//		RunState=IdleMode;
//	}
//
//
//	switch(RunState)
//	{
//	case ConfigMode:
//
//		ClearLCD();
//		SetStringFull(" 설 정 모 드");
//		DELAY_US(2000000);
//		break;
//	case RunningMode:
//		ClearLCD();
//		SetStringFull(" 실 행 모 드");
//		DELAY_US(2000000);
//		break;
//	case IdleMode:
//		ClearLCD();
//		SetStringFull(" 대 기 모 드");
//		DELAY_US(2000000);
//		break;
//	default:
//		break;
//	}

//	EPwm1Regs.AQCTLA.bit.CAD = 2;		/* Set EPWM4A on CNTR=CMPA, Down-Count */
//	EPwm1Regs.AQCTLA.bit.ZRO = 1;		/* Clear EPWM4A on CNTR=Zero */
	XIntruptRegs.XINT1CR.bit.ENABLE = 1;        // XINT1 인터럽트 : Enable
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}

interrupt void Xint2_isr(void)
{

	XIntruptRegs.XINT2CR.bit.ENABLE = 0;

//	Xint2PushCNT++;
//
//	if(Xint2PushCNT > 2)
//	{
//		Xint2PushCNT = 0;

		if(RunState==ReadyMode)
		{
			if(Mode==CurrentMode || Mode==ITSMMode || Mode==I2TMode || Mode==didtNonRepeatMode)
			{
				trigger=1;
			}
			else
			{
				repeat_Triger=1;
			}

		}

//	}




//	if(bit==1){
//		ClearLCD();
//		SetStringFull("UP");
//		DELAY_US(1000000);
//	}
//	else{
//		ClearLCD();
//		SetStringFull("DOWN");
//		DELAY_US(1000000);
//	}
	XIntruptRegs.XINT2CR.bit.ENABLE = 1;        // XINT1 인터럽트 : Enable
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}
Uint16 check=0;
interrupt void epwm1_isr(void)
{

//	EPwm1IsrTicker++;
//
//	EPwm1Regs.TBPRD = (TBCLK / PwmCarrierFrequency) - 1;
//	EPwm1Regs.CMPA.half.CMPA = (EPwm1Regs.TBPRD + 1) * PwmDutyRatioA;
//
//	/* Clear INT flag for this timer */
//	EPwm1Regs.ETCLR.bit.INT = 1;
//
//	/* Acknowledge this interrupt to receive more interrupts from group 3 */
//	PieCtrlRegs.PIEACK.bit.ACK3 = 1;



	if(g_pwmPulseCNT<=g_CNT_eq_PRD){

		g_CNT_eq_PRD = 0;
		EPwm1Regs.AQCTLA.bit.CAD = 0;		/* Set EPWM4A on CNTR=CMPA, Down-Count */
		EPwm1Regs.AQCTLA.bit.ZRO = 0;		/* Clear EPWM4A on CNTR=Zero */

	}
	else{

		g_CNT_eq_PRD++;

	}

	check++;

	EPwm1Regs.ETCLR.bit.INT = 1;
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}

//interrupt void ISR_PhaseDetect(void)			// Only MCB B
//{
//
//	unsigned int Emergency_Break_State;
//
//	Emergency_Break_State = GpioDataRegs.GPADAT.bit.GPIO0;
//
//	if(Emergency_Break_State==0)
//	{
//		PHASE_DETECT_HIGH();
//	}
//	else
//	{
//		PHASE_DETECT_LOW();
//	}
//
////	Uint16 i;
////
////    if(g_stCCDL.MCB_MODE==MCB_B)
////    {
////        SpiaRegs.SPIFFTX.bit.TXFIFO=0;                  // TX FIFO POINTER TO ZERO
////        SpiaRegs.SPIFFTX.bit.TXFIFO=1;                  // Re-Enable FIFO
////
////        SpiaRegs.SPIFFRX.bit.RXFIFORESET=0;             // RX FIFO POINTER TO ZERO
////        SpiaRegs.SPIFFRX.bit.RXFIFORESET=1;             // Re-Enable FIFO
////
////        for(i=0;i<SPI_FIFO_LEVEL;i++)
////        {
////            SpiaRegs.SPITXBUF=g_SPI_SendData[i];        // Send data
////        }
////
////    	g_stCCDL.CCDL_MCB_A_TX_READY_Int_flag = 1;
////        SpiaRegs.SPIFFTX.bit.TXFFINTCLR=1;              // Clear Interrupt flag, spi tx interrupt
////
////    	Xint1_INT_Counter++;
////    }
//
//
//	// Acknowledge this interrupt to receive more interrupts from group 1
//	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
//}
interrupt void adc_isr(void)
{
	ADC_value = AdcRegs.ADCRESULT0;

	if(g_pre_value < ADC_value)
	{
		if(ADC_value < 10000) g_wait=0;
		if(36000 < ADC_value && g_wait ==0)
		{

			GpioDataRegs.GPATOGGLE.bit.GPIO2 = 1;
			DELAY_US(10);
			GpioDataRegs.GPATOGGLE.bit.GPIO2 = 1;
			g_maincount++;
			g_wait=1;
			g_flag=1;
		}
	}

	g_pre_value = ADC_value;

//	if(!g_flag){
//		g_vector[g_count]= ADC_value;
//		g_count++;
//		if(g_count>100)
//		{
//			g_count=0;
//			g_flag=1;
//		}
//	}

	// Reinitialize for next ADC sequence
	AdcRegs.ADCTRL2.bit.RST_SEQ1 = 1;         // Reset SEQ1
	AdcRegs.ADCST.bit.INT_SEQ1_CLR = 1;       // Clear INT SEQ1 bit
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;   // Acknowledge interrupt to PIE
}



//void IdelModeRunning(){
//	Uint8 waitStatus = 1;
//
//	while(waitStatus)
//	{
//		if(isSwitchChange)							//switch change flag
//		{
//			isSwitchChange=0;
//			if(SwitchError)
//			{
//				SwitchError=0;
//				ClearLCD();
//				SetPosition(0,0);
//				SetStringFull("ERROR");
//				SetPosition(0,1);
//				SetStringFull("한개의 스위치만 ON 하세요");
//			}
//
//		}
//
//	}
//
//
//}

void ModeSwitch(Uint8 ModeValue){

	switch(ModeValue)
	{
	case 63:		//All Switch OFF
		RunState=IdleMode;
		Mode=NotSelect;
		break;
	case 31:		//Only Voltage Mode Switch ON
		RunState=RunningMode;
		Mode=VoltageMode;
		break;
	case 47:		//Only Current Mode Switch ON
		RunState=RunningMode;
		Mode=CurrentMode;
		PulseWidth = 500000;
		break;
	case 59:		//Only Di/Dt repeat Mode Switch ON
		RunState=RunningMode;
		Mode=didtRepeatMode;
		PulseWidth = 10;
//	    EALLOW;
//		SysCtrlRegs.PCLKCR0.bit.ADCENCLK = 1;
//		EDIS;

		break;
	case 55:		//Only Di/Dt not repeat Mode Switch ON
		RunState=RunningMode;
		Mode=didtNonRepeatMode;
		PulseWidth = 10;
		break;
	case 61:		//Only ITSM Mode Switch ON
		RunState=RunningMode;
		Mode=ITSMMode;
		PulseWidth = 10;
		break;
	case 62:		//Only I2T Mode Switch ON
		RunState=RunningMode;
		Mode=I2TMode;
		PulseWidth = 10;
		break;
	default:
		RunState=IdleMode;
		Mode=error;
		break;

	}
}

/*******************************************
 * Switch Detect Timer
 *
 *
 *
 *
 *
 *
 */

long PulseOutCNT=0;
interrupt void ISR_CpuTimer0(void)
{

	CpuTimer0.InterruptCount++;


//			GpioDataRegs.GPATOGGLE.bit.GPIO2 = 1;
	static Uint8 pre_SW_Status =0;		//이전 모드 변수
	static Uint8 SW_Change_Count =0;	//모드 변경 확인 카운터
	static Uint8 SetupChangeCount =0;
	static Uint8 keyLOCK = 0;
    static Uint8 lastPositionCounter =0;
    static Uint8 SwitchResetCNT = 0;



    SwitchResetCNT++;

	Uint8 SW_Status;
	Uint8 SetupSwitchStatus;
	SW_Status = GetSwitchStatus();		//switch 상태 반환
	SetupSwitchStatus = GetSetupSwitchStatus();
	/* Check Position & Direction */
	PositionCounter = EQep2Regs.QPOSCNT / 4;
	RotateDirection = EQep2Regs.QEPSTS.bit.QDF;

#if DEBUG
	SW_Status = Modevalue;
#endif

    if(preprocess==1)
    {
    	ModeSwitch(GetSwitchStatus());
    	pre_SW_Status = GetSwitchStatus();
    	preprocess=0;
    }

	if(trigger)
	{

		if(PulseOutCNT >= PulseWidth/10)
		{
			PulseOutCNT=0;
			trigger=0;
			if(Mode==CurrentMode)
			{
				GpioDataRegs.GPADAT.bit.GPIO1 = 0;
			}
			else
			{
				GpioDataRegs.GPADAT.bit.GPIO3 = 0;
			}

			RunState=Triggered;
			LCDUpdate();

		}
		else{

			PulseOutCNT++;
			if(Mode==CurrentMode)
			{
				GpioDataRegs.GPADAT.bit.GPIO1 = 1;
			}
			else
			{
				GpioDataRegs.GPADAT.bit.GPIO3 = 1;
			}

		}
	}




	if(SwitchResetCNT>10000)
	{
		SwitchResetCNT=0;
		Xint1PushCNT=0;
		Xint2PushCNT=0;
	}



	//Mode 스위치 변경 검출
	if(pre_SW_Status != SW_Status)
	{
		SW_Change_Count++;

		if(SW_Change_Count>100)
		{
			SW_Change_Count=0;
			isSwitchChange = 1;
			pre_SW_Status = SW_Status;
			ModeSwitch(SW_Status);
		}

	}

	//Setup 스위치 변경 검출
	if(!isSetupSwitchChange)
	{
		if(SetupSwitchStatus==0)
		{

			SetupChangeCount++;

			if(SetupChangeCount>10000)
			{
				SetupChangeCount=0;
				if(RunState==RunningMode)
				{
					RunState=ConfigMode;
				}
				else if(RunState==ConfigMode)
				{
					RunState=RunningMode;
				}

				keyLOCK = 1;
			}
		}
		else
		{
			if(keyLOCK==1){
				isSetupSwitchChange = 1;
				SetupChangeCount=0;
				keyLOCK=0;
			}

		}

	}


	//Wheel Data ProCess
	//*********************************************************************
	if(lastPositionCounter!=PositionCounter && isChageEncorder == 0)
	{
		if(lastPositionCounter==23)
		{
			if(RotateDirection==0 && lastPositionCounter > PositionCounter){

					isChageEncorder=2;	//++
					lastPositionCounter = PositionCounter;
			}
			else if(RotateDirection==1 && lastPositionCounter > PositionCounter)
			{

					isChageEncorder=1;	//--
					lastPositionCounter = PositionCounter;

			}
		}
		else if(lastPositionCounter==0)
		{
			if(RotateDirection==0 && lastPositionCounter < PositionCounter){

					isChageEncorder=2;	//++
					lastPositionCounter = PositionCounter;
			}
			else if(RotateDirection==1 && lastPositionCounter < PositionCounter)
			{

					isChageEncorder=1;	//--
					lastPositionCounter = PositionCounter;

			}
		}
		else
		{
			if(RotateDirection==0 && lastPositionCounter > PositionCounter){

					isChageEncorder=2;
					lastPositionCounter = PositionCounter;
			}
			else if(RotateDirection==1 && lastPositionCounter < PositionCounter)
			{

					isChageEncorder=1;
					lastPositionCounter = PositionCounter;

			}
		}


	}
	//*********************************************************************

//	g_SwitchStatus = GpioDataRegs.GPADAT.bit.GPIO31;

//	AdcRegs.ADCTRL2.bit.SOC_SEQ1 = 1;					// ADC 시퀀스 시작
//	DELAY_US(1.72L);									// ADC 시퀀스 변환시간(약1.72usec)만큼 지연
//
//	ADC_value = AdcRegs.ADCRESULT0;						// ADC 결과 저장
//
//	AdcRegs.ADCTRL2.bit.RST_SEQ1 = 1;					// ADC 시퀀서 리셋
//	DELAY_US(0.64L);

//	if(g_pre_value < ADC_value)
//	{
//		if(ADC_value < 10000) g_wait=0;
//		if(36000 < ADC_value && g_wait ==0)
//		{
//
//			GpioDataRegs.GPADAT.bit.GPIO1 = 1;
//			DELAY_US(10);
//			GpioDataRegs.GPADAT.bit.GPIO1 = 0;
//			GpioDataRegs.GPATOGGLE.bit.GPIO2 = 1;
//			DELAY_US(10);
//			GpioDataRegs.GPATOGGLE.bit.GPIO2 = 1;
//			g_maincount++;
//			g_wait=1;
//			g_flag=1;
//		}
//	}

//	g_pre_value = ADC_value;
//	g_vector[g_count]= ADC_value;
//	g_count++;
//
//	if(g_count>2048) g_count=0;



//	Toggle_Period++;
//	DataLog_Count++;
//
//	if(Toggle_Period>1000)
//	{
//		Toggle_Period=0;
//		DSP_HBEAT_TOGGLE();
//	}

//

   // Acknowledge this interrupt to receive more interrupts from group 1
   PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}
